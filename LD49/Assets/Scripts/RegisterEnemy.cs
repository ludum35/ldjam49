using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterEnemy : MonoBehaviour
{
    private void Awake()
    {
        DungeonController.Instance.m_enemies.Add(gameObject);
    }

    private void OnDestroy()
    {
        DungeonController.Instance.m_enemies.Remove(gameObject);
    }
}
