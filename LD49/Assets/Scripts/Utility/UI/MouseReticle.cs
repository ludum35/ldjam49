using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseReticle : MonoBehaviour
{
    public RectTransform Reticle;

    private void Awake()
    {
        if (Reticle == null) { Reticle = GetComponent<RectTransform>(); }
    }
    // Update is called once per frame
    void Update()
    {
        Reticle.transform.position = Input.mousePosition;
    }
}
