﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Utility
{
    public static Vector2 xz( this Vector3 v )
    {
        return new Vector2( v.x, v.z );
    }

    public static Vector3 Div( this Vector3 v, Vector3 b )
    {
        return new Vector3( v.x / b.x, v.y / b.y, v.z / b.z );
    }
}
