using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockAxis : MonoBehaviour
{
    public Vector3 angles;

    public bool LocalAxis;

    // Update is called once per frame
    void Update()
    {
        Action<Vector3> axis = (x) => transform.eulerAngles = x;
        if (LocalAxis) { axis = (x) => transform.localEulerAngles = x; }

        axis?.Invoke(angles);
    }
}
