﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetHandler : MonoBehaviour
{
    public Vector3 movePerScaleUnit;

    public Vector3 lastScale = Vector3.zero;

    public Transform scaleObject;

    public void Start()
    {
        if (scaleObject == null)
        {
            scaleObject = transform;
        }
    }

    public void FixedUpdate()
    {
        UpdateOffset();
    }

    [ContextMenu("UpdateOffset")]
    public void UpdateOffset()
    {
        //if (lastScale == scaleObject.localScale)
        //{
        //    return;
        //}
        //
        //Vector3 newScale = movePerScaleUnit;
        //transform.localPosition = new Vector3(newScale.x * scaleObject.localScale.x, newScale.y * scaleObject.localScale.y, newScale.z * scaleObject.localScale.z);
        //
        //lastScale = scaleObject.localScale;
    }
}
