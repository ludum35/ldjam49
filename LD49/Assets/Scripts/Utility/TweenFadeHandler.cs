﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TweenFadeHandler
{
    public CanvasGroup canvasGroup;
    public GameObject gameObject;
    public TweenerCore<float, float, FloatOptions> canvasFade;

    public float closeTime;
    public float openTime;

    public void Init(CanvasGroup canvasGroup, GameObject gameObject, float openTime, float closeTime)
    {
        this.canvasGroup = canvasGroup;
        this.gameObject = gameObject;
        this.closeTime = closeTime;
        this.openTime = openTime;
    }

    public void Open(bool resetAlpha = true)
    {
        gameObject.SetActive(true);

        if (resetAlpha)
        {
            canvasGroup.alpha = 0f;
        }

        DoFade(1f, openTime);
    }

    public Tween DoFade(float value, float scaleTime)
    {
        CheckFade();
        canvasFade = DOTween.To(() => canvasGroup.alpha, (x) => canvasGroup.alpha = x, value, scaleTime);
        return canvasFade;
    }

    public void Close()
    {
        DoFade(0f, closeTime).OnComplete(() => gameObject.SetActive(false));
    }

    private void CheckFade()
    {
        if (canvasFade != null)
        {
            canvasFade.Kill();
            canvasFade = null;
        }
    }
}
