using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizePitch : MonoBehaviour
{
    public Vector2 minMax = new Vector2(.9f, 1f);
    public AudioSource target;
    
    public void Randomize()
    {
        if (target == null)
        { target = GetComponent<AudioSource>(); }
        if (target == null) { return; }
        target.pitch = minMax.AsRandom();
    }

    // Start is called before the first frame update
    void Start()
    {
        Randomize();
    }
}
