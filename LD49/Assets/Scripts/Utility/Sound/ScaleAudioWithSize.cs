using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class ScaleAudioWithSize : MonoBehaviour
{
    public AudioSource source;

    public float maxSize;
    public float minSize;

    public float scaleMultiplier;

    // Update is called once per frame
    void Update()
    {
        float size = transform.localScale.magnitude * scaleMultiplier;

        var volume = Mathf.Lerp(minSize, maxSize, size);

        source.DOFade(volume, .1f);
    }
}
