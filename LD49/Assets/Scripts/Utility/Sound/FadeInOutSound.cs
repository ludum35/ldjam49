﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FadeInOutSound : MonoBehaviour
{
    public float volume;
    public float fadeTime;
    public AudioSource source;
    public bool FadeInOnStart = true;
    public bool TakeStartVolume = false;

    // Start is called before the first frame update
    void Start()
    {
        if (source == null)
        {
            source = GetComponent<AudioSource>();
        }

        if (TakeStartVolume)
        {
            volume = source.volume;
        }

        if (FadeInOnStart)
        {
            Play();
        }
    }

    public void Play()
    {
        source.volume = 0f;

        source.DOFade(volume, fadeTime);
        source.Play();
    }
    public void Stop()
    {
        source.DOFade(0f, fadeTime);
    }
}
