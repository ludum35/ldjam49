﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioClipContainer : MonoBehaviour
{
    public List<AudioClip> clips = new List<AudioClip>();

    public bool PlayOnAwake = false;

    public UnityEvent OnBeforePlay;

    private void Awake()
    {
        PlayOnAwakeHandler();
    }

    private void PlayOnAwakeHandler()
    {
        if (!PlayOnAwake)
        {
            return;
        }

        AudioSource source = null;
        if (source = GetComponent<AudioSource>())
        {
            PlayRandom(source);
        }
    }

    public void PlayRandom()
    {
        PlayRandom(null);
    }

    public void PlayRandom(AudioSource source)
    {
        if (source == null)
        {
            source = GetComponent<AudioSource>();
            if (source == null)
            {
                source = gameObject.AddComponent<AudioSource>();
            }
        }

        OnBeforePlay?.Invoke();

        source?.PlayOneShot(clips.Random());
    }
}
