using System;
using System.Collections.Generic;
using System.Linq;

public class NPCChoiceTable
{
    public struct Entry
    {
        public int weight;
        public Action callback;
    }

    private List<Entry> m_entries = new List<Entry>();

    public void AddChoice(int weight, Action callback)
    {
        Entry entry = new Entry() { weight = weight, callback = callback };
        m_entries.Add(entry);
    }

    public void Execute()
    {
        int total = m_entries.Sum(e => e.weight);
        int target = UnityEngine.Random.Range(0, total);
        int N = 0;

        for (int i = 0; i < m_entries.Count; i++)
        {
            N += m_entries[i].weight;
            if (N > target)
            {
                m_entries[i].callback.Invoke();
                return;
            }
        }
    }
}