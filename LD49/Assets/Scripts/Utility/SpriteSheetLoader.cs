﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class SpriteSheetLoader
{
    //-----sprite parser stuff?
    public static Sprite LoadSingleSprite(string sprite)
    {
        return Resources.Load<Sprite>(sprite);
    }

    public static Sprite LoadSingleFromSheet(string sprite)
    {
        List<Sprite> Sprites = LoadSheet(sprite, out int index);


        if (Sprites == null || index >= Sprites.Count)
        {
            return null;
        }

        return Sprites[index];
    }

    public static List<Sprite> LoadSheet(string sprite, out int index)
    {
        index = -1;

        if (sprite == null)
        {
            return null;
        }

        string[] fullName = sprite.Split('_');
        string parentName = sprite;

        if (fullName.Length == 1)
        {
            index = 0;
            return new List<Sprite>() { LoadSingleSprite(sprite) };
        }

        parentName = parentName.TrimEnd(
            ("_" + fullName[fullName.Length - 1])
            .ToCharArray());

        List<char> indexChars = fullName[fullName.Length - 1].ToCharArray().ToList();

        for (int i = 0; i < indexChars.Count; i++)
        {
            if (!char.IsNumber(indexChars[i]))
            {
                indexChars.RemoveAt(i);
                i--;
            }
        }

        string indexString = string.Concat(indexChars);

        index = int.Parse(indexString);

        Sprite[] sprites = Resources.LoadAll<Sprite>(parentName);

        return sprites.ToList();
    }
}
