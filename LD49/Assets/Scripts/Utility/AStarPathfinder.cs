﻿using System.Collections.Generic;
using UnityEngine;

public interface IAStarGenericNode
{
    Vector3 GetPosition();
}

public class AStarPathfinder
{
    public delegate List<IAStarGenericNode> GetNeighborsFnc( IAStarGenericNode parent, IAStarGenericNode current );    

    public delegate float GetFScoreFnc( IAStarGenericNode from, IAStarGenericNode to );
    public delegate float GetGScoreFnc( IAStarGenericNode from, IAStarGenericNode to );

    public GetNeighborsFnc GetNeighbors { get; set; }
    public GetFScoreFnc GetFScore { get; set; }
    public GetGScoreFnc GetGScore { get; set; }

    protected IAStarGenericNode CurrentTarget = null;
    protected IAStarGenericNode CurrentSource = null;

    public AStarPathfinder()
    {
        GetFScore = DefaultFScore;
        GetGScore = DefaultGScore;
    }

    public float DefaultFScore( IAStarGenericNode from, IAStarGenericNode to )
    {
        return Vector3.Distance( from.GetPosition(), to.GetPosition() );
    }

    public float DefaultGScore( IAStarGenericNode from, IAStarGenericNode to )
    {
        return Vector3.Distance( from.GetPosition(), to.GetPosition() );
    }

    protected bool CheckIsInitialized()
    {
        return GetNeighbors != null && GetFScore != null && GetGScore != null;
    }    

    public virtual List<IAStarGenericNode> Find( IAStarGenericNode start, IAStarGenericNode end )
    {
        CurrentSource = start;
        CurrentTarget = end;

        HashSet<IAStarGenericNode> closed = new HashSet<IAStarGenericNode>();
        Dictionary<IAStarGenericNode, PathfindingNodeInternal<Vector3, IAStarGenericNode>> open = new Dictionary<IAStarGenericNode, PathfindingNodeInternal<Vector3, IAStarGenericNode>>();
        PriorityQueue<PathfindingNodeInternal<Vector3, IAStarGenericNode>> openPQ = new PriorityQueue<PathfindingNodeInternal<Vector3, IAStarGenericNode>>();

        float f = GetFScore( start, end );
        PathfindingNodeInternal<Vector3, IAStarGenericNode> startNode = new PathfindingNodeInternal<Vector3, IAStarGenericNode>( start.GetPosition(), start, f, 0.0f, null );
        open.Add( start, startNode );
        openPQ.Add( f, startNode );
        
        while( open.Count > 0 )
        {
            if( open.Count > 1000 )
            {
                Debug.LogError( "Too many open nodes in pathfinder (>10000), possible bug in f/gscore or getneighbors" );
                break;
            }

            PathfindingNodeInternal<Vector3, IAStarGenericNode> current = openPQ.RemoveMin();
            open.Remove( current.identifier );

            if( current.identifier == end )
            {
                List<IAStarGenericNode> path = new List<IAStarGenericNode>();
                path.Add( current.identifier );
                while( current.cameFrom != null )
                {
                    current = current.cameFrom;
                    path.Insert( 0, current.identifier );
                }
                return path;
            }

            closed.Add( current.identifier );

            IAStarGenericNode from = current.cameFrom == null ? current.identifier : current.cameFrom.identifier;
            List<IAStarGenericNode> neighbors = GetNeighbors( from, current.identifier );

            foreach( IAStarGenericNode neighbor in neighbors )
            {
                if( closed.Contains( neighbor ) )
                    continue;

                float tg = current.gScore + GetGScore( current.identifier, neighbor );

                bool contains = open.ContainsKey( neighbor );
                if( !contains || tg < open[neighbor].gScore )
                {
                    PathfindingNodeInternal<Vector3, IAStarGenericNode> neighborNode = contains ? 
                        open[neighbor] :
                        new PathfindingNodeInternal<Vector3, IAStarGenericNode>( neighbor.GetPosition(), neighbor, tg + GetFScore( neighbor, end ), tg, current );

                    if( !contains )
                    {
                        open.Add( neighbor, neighborNode );
                        openPQ.Add( neighborNode.fScore, neighborNode );
                    }
                }
            }
        }

        return null;
    }
}
