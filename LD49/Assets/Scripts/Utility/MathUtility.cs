﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class MathUtility
{
    public static void Swap<T>( ref T lhs, ref T rhs )
    {
        T temp = lhs;
        lhs = rhs;
        rhs = temp;
    }

    public static int Mod( int x, int m )
    {
        int r = x % m;

        return r < 0 ? r + m : r;
    }

    public static Vector2 ClosestPointOnSegment( Vector2 point, Vector2 A, Vector2 B )
    {
        float length2 = ( B - A ).sqrMagnitude;

        if( length2 == 0.0 ) return A;

        float t = Mathf.Clamp01( Vector2.Dot( point - A, B - A ) / length2 );

        return A + t * ( B - A );
    }

    public static T Random<T>( this List<T> values )
    {
        return values[UnityEngine.Random.Range( 0, values.Count )];
    }

    // TODO: Inconsistent and probably bad performance.
    // Could theorethically hang forever.
    public static T RandomExcept<T>( this List<T> values, T ignore )
    {
        if( values.Count - 1 == 0 )
            throw new InvalidOperationException( "List is too small for operation." );

        T value;
        do
        {
            value = Random( values );
        }
        while( value.Equals( ignore ) );

        return value;
    }
}
