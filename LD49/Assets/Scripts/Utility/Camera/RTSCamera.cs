using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSCamera : MonoBehaviour
{
    public enum Axis
    {
        X = 0,
        Y = 1,
        Z = 2
    }

    public enum ModeEnum
    {
        Ortho,
        Perspective
    }

    public BoxCollider BoundsCollider;

    public float scrollSpeed;
    public float zoomSpeed;

    public float scrollDampening;
    public float zoomDampening;

    public Vector3 CameraMovement = Vector3.zero;

    [Header("Ortho")]
    public Vector2 MinMaxZoom = new Vector2(8f, 40f);
    public float CameraHeight = 100f;
    public bool InvertZoom = false;

    public bool FreezeHorizontal = true;

    [Space, Header("Mode")]
    public ModeEnum Mode = ModeEnum.Ortho;
    public Camera Camera;

    private void Start()
    {
        if (Camera == null) { Camera = Camera.main; }

        if (BoundsCollider == null)
        {
            Debug.LogWarning("No bounds set for camera.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCamera();
    }

    public Vector3 GetInput()
    {
        float zoom = -100f;

        if (InvertZoom)
        {
            zoom *= -1f;
        }

        float h = 0f;
        float v = 0f;

        if (!FreezeHorizontal)
        {
            h = Input.GetAxisRaw("Horizontal") * scrollSpeed;
            v = Input.GetAxisRaw("Vertical") * scrollSpeed;
        }

        float scroll = Input.GetAxisRaw("Mouse ScrollWheel") * zoom * zoomSpeed;

        return new Vector3(h, scroll, v);
    }

    public float GetOrthoCamZoom(float zoomInput)
    {
        return Mathf.Clamp(Camera.orthographicSize + zoomInput, MinMaxZoom.x, MinMaxZoom.y);
    }

    public Vector3 UpdateOrthoCamera(Vector3 camMovement)
    {
        float newOrthoSize = GetOrthoCamZoom(camMovement.y);

        float camSpeed = (newOrthoSize - MinMaxZoom.x) / (MinMaxZoom.y - MinMaxZoom.x);

        Camera.orthographic = true;

        Camera.orthographicSize = newOrthoSize;

        Vector3 newPos = transform.position + camMovement * Mathf.Lerp(1.0f, 5.0f, camSpeed);

        newPos.y = CameraHeight;

        return newPos;
    }

    private Vector3 UpdatePerspectiveCamera(Vector3 camMovement)
    {
        //todo? needs nothing else?
        Camera.orthographic = false;

        return transform.position + camMovement;
    }

    public void UpdateCamera()
    {
        Vector3 inputs = GetInput();

        CameraMovement += inputs;

        Vector3 ContainedPos = GetCameraUpdate(Mode, CameraMovement);

        for (int i = 0; i < 3; i++)
        {
            ContainedPos[i] = LimitAxis(ContainedPos[i], (Axis)i);
        }

        CameraMovement.x = CameraMovement.x * scrollDampening;
        CameraMovement.y = CameraMovement.y * zoomDampening;
        CameraMovement.z = CameraMovement.z * scrollDampening;

        transform.position = ContainedPos;
    }

    private Vector3 GetCameraUpdate(ModeEnum mode, Vector3 cameraMovement)
    {
        if (Mode == ModeEnum.Ortho)
        {
            return UpdateOrthoCamera(CameraMovement);
        }

        if (mode == ModeEnum.Perspective)
        {
            return UpdatePerspectiveCamera(CameraMovement);
        }

        return UpdateOrthoCamera(CameraMovement);
    }


    public float LimitAxis(float value, Axis axis)
    {
        if (BoundsCollider == null)
        {
            return value;
        }

        Bounds b = BoundsCollider.bounds;

        Vector3 Min = BoundsCollider.bounds.center - BoundsCollider.bounds.extents;
        Vector3 Max = BoundsCollider.bounds.center + BoundsCollider.bounds.extents;

        return Mathf.Min(Max[(int)axis],
                Mathf.Max(Min[(int)axis], value));
    }
}
