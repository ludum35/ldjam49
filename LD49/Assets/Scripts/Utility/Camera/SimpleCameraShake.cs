using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCameraShake : Singleton<SimpleCameraShake>
{
    //Camera Camera;
    public Vector3 shakeVelocity;
    public Vector3 shakeMagnitude;
    public float decay = 15f;
    public Vector3 restPosition;

    // Start is called before the first frame update
    public override void Awake()
    {
        base.Awake();

        restPosition = transform.localPosition;

        //Camera = Camera.main;
    }

    private void LateUpdate()
    {
        var centerDir = restPosition - transform.localPosition;

        transform.localPosition += centerDir * Time.deltaTime * decay;

        shakeMagnitude *= (1f - Mathf.Clamp01(Time.deltaTime * decay));
        shakeVelocity *= (1f - Mathf.Clamp01(Time.deltaTime * decay));

        var shakeDir = Vector3.zero;

        for (int i = 0; i < 3; i++)
        {
            shakeDir[i] = Random.Range(-shakeMagnitude[i], shakeMagnitude[i]);
        }

        shakeVelocity += shakeDir;

        transform.position += shakeVelocity * Time.deltaTime;
    }

    public void AddShake(float magnitude)
    {
        AddShake(Vector3.one.x0z(), magnitude);
    }

    public void AddShake(Vector3 dir, float magnitude)
    {
        dir = dir.x0z(); // no y movement.

        dir *= magnitude;

        shakeMagnitude += dir;
    }
}
