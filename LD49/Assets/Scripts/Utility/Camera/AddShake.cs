using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddShake : MonoBehaviour
{
    public float Magnitude;

    void Start()
    {
        var shake = SimpleCameraShake.Instance;
        if (shake != null)
        {
            shake.AddShake(Magnitude);
        }
    }
}
