using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public struct Vector2i
{
    public static readonly Vector2i[] OtherAxes = new Vector2i[]
    {
        new Vector2i( 2, 1 ), new Vector2i( 0, 2 ), new Vector2i( 0, 1 )
    };

    public static readonly Vector2i[] Neighbors = new Vector2i[]
    {
        new Vector2i( 0, -1 ), new Vector2i( -1, 0 ), new Vector2i( 1, 0 ), new Vector2i( 0, 1 )
    };

    public static readonly Vector2i zero = new Vector2i( 0, 0 );
    public static readonly Vector2i one = new Vector2i( 1, 1 );
    public static readonly Vector2i up = new Vector2i( 0, 1 );
    public static readonly Vector2i right = new Vector2i( 1, 0 );
    public static readonly Vector2i down = new Vector2i( 0, -1 );
    public static readonly Vector2i left = new Vector2i( -1, 0 );

    public int x;
    public int y;

    public int this[int key]
    {
        get
        {
            switch( key )
            {
                default:
                case 0: return x;
                case 1: return y;
            }
        }
        set
        {
            switch( key )
            {
                case 0:
                    x = value;
                    break;
                case 1:
                    y = value;
                    break;
            }
        }
    }


    public Vector2i( int _x, int _y )
    {
        x = _x;
        y = _y;
    }

    public Vector2i( Vector2 v )
    {
        x = (int)v.x;
        y = (int)v.y;
    }

    public Vector2i( int v )
    {
        x = v;
        y = v;
    }

    public static implicit operator Vector2i( Int2 i)
    {
        return new Vector2i( i.x, i.y );
    }

    public static explicit operator Vector2( Vector2i v )
    {
        return new Vector2( v.x, v.y );
    }

    public double magnitude
    {
        get { return Math.Sqrt( (double)( x * x + y * y ) ); }
    }

    public static Vector2i operator +( Vector2i a, Vector2i b )
    {
        return new Vector2i( a.x + b.x, a.y + b.y );
    }

    public static Vector2i operator -( Vector2i a, Vector2i b )
    {
        return new Vector2i( a.x - b.x, a.y - b.y );
    }

    public static Vector2i operator -( Vector2i a )
    {
        return new Vector2i( -a.x, -a.y );
    }

    public static Vector2i operator /( Vector2i a, Vector2i b )
    {
        return new Vector2i( a.x / b.x, a.y / b.y );
    }

    public static Vector2i operator /( Vector2i a, int b )
    {
        return new Vector2i( a.x / b, a.y / b );
    }

    public static Vector2i operator *( Vector2i a, Vector2i b )
    {
        return new Vector2i( a.x * b.x, a.y * b.y );
    }

    public override bool Equals( object obj )
    {
        if( !( obj is Vector2i ) )
            return false;

        Vector2i other = (Vector2i)obj;

        return x == other.x && y == other.y;
    }

    public static bool operator !=( Vector2i a, Vector2i b )
    {
        return a.x != b.x || a.y != b.y;
    }

    public static bool operator ==( Vector2i a, Vector2i b )
    {
        return a.x == b.x && a.y == b.y;
    }

    public override int GetHashCode()
    {
        return x + y * ( 1 << 16 );
    }

    public static Vector2i GetOtherAxes( int axis )
    {
        return OtherAxes[axis];
    }

    public static Vector2i GetNeighbor( int index )
    {
        return Neighbors[index];
    }

    public static int MajorAxis( Vector2i v )
    {
        return Math.Abs( v.x ) > Math.Abs( v.y ) ? 0 : 1;
    }

    public static void MakeMinMax( ref Vector2i a, ref Vector2i b )
    {
        if( a.x > b.x )
        {
            MathUtility.Swap( ref a.x, ref b.x );
        }
        if( a.y > b.y )
        {
            MathUtility.Swap( ref a.y, ref b.y );
        }
    }

    

    public override string ToString()
    {
        return string.Format( "({0}, {1})", x, y );
    }
}
