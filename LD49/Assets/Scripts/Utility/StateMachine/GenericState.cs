﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class GenericState : MonoBehaviour
{
    private GenericStateController _controller { get; set; }

    public List<Func<bool>> EntryConditions = new List<Func<bool>>();

    public bool Active { get; private set; }
    public bool IsBusy => EvaluateBusy();

    public virtual void EnterState() { OnEnterState?.Invoke(); }

    public virtual void ExitState() { OnExitState?.Invoke(); }

    protected virtual void Update() { }

    public virtual void OnPreUpdate() { }

    public virtual void UpdateBehaviour() { }

    public UnityEvent OnEnterState, OnExitState;
    
    public virtual void Init(GenericStateController controller)
    {
        _controller = controller;

        _controller.stateChange += OnStateChanged;
    }

    protected virtual bool EvaluateBusy()
    {
        return false;
    }

    protected virtual void OnStateChanged(GenericState obj)
    {
        if (obj == this)
        {
            Active = true;
            return;
        }

        Active = false;
    }

    protected virtual void SwitchState(GenericState newState, bool checkEntry = false)
    {
        if (checkEntry && !newState.CanEnter())
        {
            return;
        }

        _controller.SetState(newState);
    }

    private bool CanEnter()
    {
        foreach (Func<bool> t in EntryConditions)
        {
            if (!t.Invoke())
            { return false; }
        }
        return true;
    }

    protected virtual void RegisterTrigger<T, T1>(T handler) where T : Delegate
    {
        Func<bool> preCheck = () =>
        {
            if (Active)
            {
                return true;
            }

            return false;
        };

        _controller.RegisterTrigger<T, T1>(handler, preCheck);
    }
}
