﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraShake : MonoBehaviour
{
    public Transform _Camera;

    public void Awake()
    {
        if (_Camera == null)
        {
            _Camera = FindObjectOfType<Camera>().transform;
        }
    }

    public void AddShake(float magnitude, int freq, float time)
    {
        if (_Camera == null) { Debug.LogError("No camera set for shaking!"); }

        var t = _Camera.DOShakeRotation(time, magnitude, freq);
        t.OnComplete(() => _Camera.transform.DOLocalRotate(Vector3.zero, .1f));
    }
}
