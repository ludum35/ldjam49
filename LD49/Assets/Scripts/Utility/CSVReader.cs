﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public static class CSVReader
{
    private static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
    private static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
    private static char[] TRIM_CHARS = { '\"' };

    public static List<Dictionary<string, object>> Read(string file)
    {
        TextAsset data = Resources.Load (file) as TextAsset;

        return ReadText(data.text);
    }

    public static List<Dictionary<string, object>> ReadText(string text)
    {
        List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

        string[] lines = Regex.Split(text, LINE_SPLIT_RE);

        if (lines.Length <= 1)
        {
            return list;
        }

        string[] header = Regex.Split(lines[0], SPLIT_RE);

        header = processNestedValues(header);

        for (int i = 1; i < lines.Length; i++)
        {
            string[] values = Regex.Split(lines[i], SPLIT_RE);
            values = processNestedValues(values);

            if (values.Length == 0 || values[0] == "")
            {
                continue;
            }

            Dictionary<string, object> entry = new Dictionary<string, object>();
            for (int j = 0; j < header.Length && j < values.Length; j++)
            {
                string value = values[j];
                value = value.TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\", "");
                object finalvalue = value;
                int n;
                float f;
                bool b;
                if (bool.TryParse(value, out b))
                {
                    finalvalue = b;
                }
                if (int.TryParse(value, out n))
                {
                    finalvalue = n;
                }
                else if (float.TryParse(value, out f))
                {
                    finalvalue = f;
                }
                entry[header[j]] = finalvalue;
            }
            list.Add(entry);
        }
        return list;
    }

    public static string[] processNestedValues(string[] header)
    {
        //problem: header gets split on "," through the curly brackets.
        //solution: merge the header back together by finding the opening and closing brackets.
        //example Size{x,y}
        // you want size{x then comma, then y};
        int OpenBrackets = 0;

        List<string> processedHeaders = new List<string>(header.Length);
        string currentHeader = "";
        bool isOpen = false;

        for (int i = 0; i < header.Length; i++)
        {
            OpenBrackets += header[i].Count(c => c == '{');
            OpenBrackets -= header[i].Count(c => c == '}');

            if (OpenBrackets > 0)
            {
                isOpen = true;
                currentHeader += header[i] + ",";
                continue;
            }

            //all brackets have been closed, meaning this is the final header concated;
            if (isOpen)
            {
                isOpen = false;
            }

            currentHeader += header[i];

            //not good removing the quotes. (should remove extra quotes around object)
            currentHeader = currentHeader.Trim('"');

            processedHeaders.Add(currentHeader);
            currentHeader = "";
        }

        return processedHeaders.ToArray();
    }

    public static bool IsValue(string value)
    {
        if (bool.TryParse(value, out bool b))
        {
            return true;
        }
        if (int.TryParse(value, out int n))
        {
            return true;
        }
        else if (float.TryParse(value, out float f))
        {
            return true;
        }

        return false;
    }

    public static bool IsArray(string v)
    {
        return v.Contains("|ARRAY");
    }

    public static bool IsReference(string v)
    {
        return v.Contains("|REF");
    }

    public static bool IsLiteral(string v)
    {
        return v.Contains("|LITERAL");
    }

    public static bool IsObject(string header, out string headerString, out string result)
    {
        int pFrom = header.IndexOf("{");
        headerString = "";

        result = Clean(header);

        if (!string.IsNullOrEmpty(result))
        {
            headerString = header.Substring(0, pFrom);
            return true;
        }

        return false;
    }

    public static string Clean(string header)
    {
        int pFrom = header.IndexOf("{") + 1;
        int pTo = header.LastIndexOf("}");
        string result = "";

        if (pFrom != 0 && pTo != 0)
        {
            result = header.Substring(pFrom, pTo - pFrom);
        }

        return result;
    }

    //use on values, not headers.
    public static string CleanNestedContent(string values)
    {
        char[] charArray = values.ToCharArray();
        int bracketsOpen = 0;

        for (int i = 0;i < charArray.Length;i++)
        {
            if (charArray[i] == '{')
            {
                bracketsOpen++;
            }
            if (charArray[i] == '}')
            {
                bracketsOpen--;
            }
            if (bracketsOpen == 0)
            {
                if (charArray[i] == ',')
                {
                    charArray[i] = '\n';
                }
            }
        }
        string newString = new string(charArray);
        string[] newValues = newString.Split('\n');

        for (int i = 0; i < newValues.Length;i++)
        {
            newValues[i] = Clean(newValues[i]);
        }
        newString = "";

        foreach (string s in newValues)
        {
            newString += s + "\n";
        }

        return newString;
    }
}