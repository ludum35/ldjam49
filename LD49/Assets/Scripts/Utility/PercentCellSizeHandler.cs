﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PercentCellSizeHandler : MonoBehaviour
{
    GridLayoutGroup layout;
    RectTransform rectT;

    public int CellsPerColumn;
    public int CellsPerRow;

    // Start is called before the first frame update
    void Start()
    {
        layout = GetComponent<GridLayoutGroup>();
        rectT = transform.parent.GetComponent<RectTransform>();
    }

    private void SetSize()
    {
        float sizeX = rectT.rect.width;
        float sizeY = rectT.rect.height;

        float cellSizeX = sizeX / CellsPerRow;
        float cellSizeY = sizeY / CellsPerColumn;

        float spacingX = layout.spacing.x;
        float spacingY = layout.spacing.y;

        cellSizeX -= (spacingX);
        cellSizeY -= (spacingY);

        layout.cellSize = new Vector2(cellSizeX, cellSizeY);
    }

    // Update is called once per frame
    void Update()
    {
        SetSize();
    }
}
