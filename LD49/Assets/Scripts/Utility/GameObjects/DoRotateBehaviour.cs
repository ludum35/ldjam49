using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoRotateBehaviour : MonoBehaviour
{
    public float Time;
    public Vector3 Angle;
    public float Delay;

    private void Start()
    {
        transform.DOBlendableLocalRotateBy(Angle, Time, RotateMode.LocalAxisAdd).SetDelay(Delay);
    }
}
