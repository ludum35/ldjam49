using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Events;

public class DeparentAndFollow : MonoBehaviour
{
    public Transform Follow;
    public UnityEvent OnFollowDestroyed;
    public Vector3 lastPos;

    float startTime;

    private void Start()
    {
        if (Follow == null) { Follow = transform.parent; }

        startTime = Time.time;

        var pos = transform.position;

        var t_parent = transform.parent;

        transform.SetParent(null);
    }

    private void FixedUpdate()
    {
        if(Follow != null)
            transform.position = Follow.position;
    }

    private void LateUpdate()
    {
        if (Follow == null || Follow.gameObject == null || !IsActive())
        {
            Follow = null;
            transform.position = lastPos;
            OnFollowDestroyed?.Invoke();
        }
        if (Follow != null)
        {
            lastPos = transform.position;
        }
    }

    private bool IsActive()
    {
        if (Time.time - startTime < 2f) { return true; }
        if (Follow?.gameObject == null) { return false; }
        return Follow.gameObject.activeSelf;
    }
}
