using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoScaleBehaviour : MonoBehaviour
{
    public Vector3 Scale;
    public float Delay;
    public float TimeToScale;

    public void Start()
    {
        transform.DOScale(Scale, TimeToScale).SetDelay(Delay);
    }
}
