using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DeparentOnDestroy : MonoBehaviour
{
    public GameObject DeathFX;

    public Transform[] Deparent;

    private void OnDestroy()
    {
        RemoveParents();
    }

    private void RemoveParents()
    {
        //transform.DetachChildren();
        Instantiate(DeathFX, transform.position, transform.rotation);
        foreach (var k in Deparent)
        {
            if (k == null) { continue; }
            k.transform.SetParent(null);
            
        }
    }
}
