using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class FadeOverTime : MonoBehaviour
{
    public float Time;
    public bool DestroyAfterFade = true;

    public Vector3 FadeAxis = Vector3.zero;
    private void Start()
    {
        transform.DOScale(FadeAxis, Time).OnComplete(() => OnFaded());
    }

    private void OnFaded()
    {
        if (DestroyAfterFade)
        {
            Destroy(gameObject);
        }
    }
}
