using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerAfterTime : MonoBehaviour
{
    [SerializeField]
    public List<AfterTimeTrigger> Triggers = new List<AfterTimeTrigger>();

    [Serializable]
    public class AfterTimeTrigger
    {
        public float Time;
        public UnityEvent Action;
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach (var k in Triggers)
        {
            this.InvokeDelayed(k.Time, () => k.Action?.Invoke());
        }
    }
}
