using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnEnabled : MonoBehaviour
{
    public GameObject Spawn;
    public bool Parent;

    public void OnEnable()
    {
        Transform parent = transform;
        if (!Parent) { parent = null; }
        if (Spawn == null) { return; }
        Instantiate(Spawn, transform.position, Quaternion.identity, parent);
    }
}
