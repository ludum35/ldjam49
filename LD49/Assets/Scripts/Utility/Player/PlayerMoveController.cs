using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerMoveController : MonoBehaviour
{
    public float MovementSpeed;
    public float VelocityResistanceMultiplier = 10f;

    public CharacterController characterController;

    public Vector3 Velocity;

    public Transform playerTransform;

    public bool RelativeMovement = false;
    public bool UseVelocity = true;

    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        if (playerTransform == null) { playerTransform = transform; }
    }

    // Update is called once per frame
    void Update()
    {
        //Thread.Sleep(0);

        UpdateMovement();
    }

    private void FixedUpdate()
    {
        FixedMovement();
    }

    private Vector3 GetMovement()
    {
        Vector3 movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            movement.z += 1f;
        }

        if (Input.GetKey(KeyCode.S))
        {
            movement.z -= 1f;
        }

        if (Input.GetKey(KeyCode.A))
        {
            movement.x -= 1f;
        }

        if (Input.GetKey(KeyCode.D))
        {
            movement.x += 1f;
        }

        return movement;
    }

    void FixedMovement()
    {
        //Velocity *= (1f - Mathf.Clamp01(Time.fixedDeltaTime * VelocityResistanceMultiplier));
    }

    void UpdateMovement()
    {
        var pos = characterController.transform.position;
        characterController.transform.position = pos.x0z();

        Vector3 movement = GetMovement();

        if (RelativeMovement) { movement = playerTransform.TransformDirection(playerTransform.position + movement); }

        var dir = movement.normalized * MovementSpeed * Time.deltaTime;

        var nextMove = dir;

        if (UseVelocity)
        {
            Velocity *= (1f - Mathf.Clamp01(Time.deltaTime * VelocityResistanceMultiplier));

            Velocity += dir;

            nextMove = Velocity * Time.deltaTime;
        }

        Vector3 moveTo = playerTransform.position + nextMove;

        characterController.Move(nextMove);
    }
}
