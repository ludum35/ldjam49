using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDirectionProvider
{
    Vector3 Direction { get; }
}

public class DirectionProvider : IDirectionProvider
{
    public Vector3 Direction => Vector3.zero;
}
