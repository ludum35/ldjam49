using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMouseAim : MonoBehaviour, IDirectionProvider
{
    public Camera Camera;
    public Transform Player;
    public Vector3 hitPos;

    public Vector3 Direction { get; set; }
    public float angle;

    public bool RotatePlayerSprite = false;

    public Transform cameraTarget;
    public float cameraMoveDist;

    public void Start()
    {
        if (Camera == null) { Camera = Camera.main; }
    }

    public void Update()
    {
        UpdateDirection();
        if (RotatePlayerSprite) { RotatePlayer(); }
    }

    public void UpdateDirection()
    {
        Direction = GetWorldAimDirection().x0z() - transform.position.x0z();
        var moveDist = Mathf.Lerp(0f, cameraMoveDist, Direction.magnitude / 15f);
        cameraTarget.transform.localPosition = Direction.normalized * moveDist;
    }

    public void RotatePlayer()
    {
        angle = Vector3.SignedAngle(Player.transform.up, Direction, Vector3.up);
        Player.transform.localEulerAngles += new Vector3(0f, 0f, angle);// Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public Vector3 GetWorldAimDirection()
    {
        hitPos = Camera.ScreenToWorldPoint(Input.mousePosition).x0z();
        return hitPos;
    }

    //public Vector2 GetScreenAimDirection()
    //{
    //    var player = Camera.WorldToScreenPoint(transform.position);
    //    var mouse = Input.mousePosition;
    //    var dir = mouse - player;

    //    return dir;
    //}
}
