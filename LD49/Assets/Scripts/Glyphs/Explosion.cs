﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Explosion : Glyph, IGlyphFireTrigger, IGlyphHitTrigger
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        GameObject explosion = SpawnPool.Instance.Instantiate(evt.tree.m_explosionPrefab);
        explosion.transform.position = evt.origin;
        explosion.transform.forward = evt.direction;
        explosion.transform.localScale = Vector3.one * evt.GetSizeMod();

        GlyphBehaviour glyphBehaviour = explosion.GetComponent<GlyphBehaviour>();
        glyphBehaviour.IntializeGlyph(this, evt);
    }

    public override string GetName()
    {
        return "Explosion";
    }
    public override string GetDescription()
    {
        return "A circular explosion. Does not travel.";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.ExplosionSprite;
    }
}
