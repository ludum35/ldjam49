﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Fireball : Glyph, IGlyphFireTrigger, IGlyphHitTrigger, IGlyphHitWallTrigger
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        float size = evt.GetSizeMod();

        GameObject fireball = SpawnPool.Instance.Instantiate(evt.tree.m_fireballPrefab);
        fireball.transform.position = evt.origin + evt.direction * Mathf.Max(0.0f, size - 1.0f);
        fireball.transform.forward = evt.direction;
        fireball.transform.localScale = Vector3.one * size;

        fireball.GetComponent<Rigidbody>().velocity = 
            evt.GetInaccuracy(6.0f) * evt.direction * 
            (evt.tree.m_fireballSpeed * evt.GetSpeedMod());

        GlyphBehaviour glyphBehaviour = fireball.GetComponent<GlyphBehaviour>();
        glyphBehaviour.IntializeGlyph(this, evt);
    }

    public override string GetName()
    {
        return "Fireball";
    }
    public override string GetDescription()
    {
        return "";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.FireballSprite;
    }
}
