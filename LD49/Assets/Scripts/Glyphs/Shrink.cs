﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Shrink : Glyph
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        evt.SizeMod *= 0.9f;
    }
    public override string GetName()
    {
        return "Shrink";
    }

    public override string GetDescription()
    {
        return "Makes all projectile glyphs smaller on this chain and all chains below.";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.ShrinkSprite;
    }
}
