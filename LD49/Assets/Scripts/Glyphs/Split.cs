﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Split : Glyph, IGlyphOnFireListener, IDoNotInherit
{
    public enum SplitShape
    {
        Diagonal2,
        Diagonal4,
        Side
    }

    public SplitShape Shape { get; set; }

    public bool m_blockRetrigger;

    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        // During the duration of this trigger, we do not want to retrigger because this will cause an infinite loop.
        if(!m_blockRetrigger)
        {
            m_blockRetrigger = true;
            if(source == TriggerSource.Fire)
            {
                CastSpellEvent childEvt;

                switch(Shape)
                {
                    case SplitShape.Diagonal2:
                        childEvt = evt.Copy();
                        childEvt.direction = Quaternion.AngleAxis(30.0f, Vector3.up) * evt.GetInaccuracy(0.0f) * evt.direction;        
                        childEvt.currentChain.Trigger(childEvt);

                        childEvt = evt.Copy();
                        childEvt.direction = Quaternion.AngleAxis(-30.0f, Vector3.up) * evt.GetInaccuracy(0.0f) * evt.direction;
                        childEvt.currentChain.Trigger(childEvt);
                        break;

                    case SplitShape.Diagonal4:
                        childEvt = evt.Copy();
                        childEvt.direction = Quaternion.AngleAxis(20.0f, Vector3.up) * evt.GetInaccuracy(0.0f) * evt.direction;
                        childEvt.currentChain.Trigger(childEvt);

                        childEvt = evt.Copy();
                        childEvt.direction = Quaternion.AngleAxis(40.0f, Vector3.up) * evt.GetInaccuracy(0.0f) * evt.direction;
                        childEvt.currentChain.Trigger(childEvt);

                        childEvt = evt.Copy();
                        childEvt.direction = Quaternion.AngleAxis(-20.0f, Vector3.up) * evt.GetInaccuracy(0.0f) * evt.direction;
                        childEvt.currentChain.Trigger(childEvt);

                        childEvt = evt.Copy();
                        childEvt.direction = Quaternion.AngleAxis(-40.0f, Vector3.up) * evt.GetInaccuracy(0.0f) * evt.direction;
                        childEvt.currentChain.Trigger(childEvt);
                        break;

                    case SplitShape.Side:
                        childEvt = evt.Copy();
                        childEvt.direction = Quaternion.AngleAxis(80.0f, Vector3.up) * evt.GetInaccuracy(0.0f) * evt.direction;
                        childEvt.currentChain.Trigger(childEvt);

                        childEvt = evt.Copy();
                        childEvt.direction = Quaternion.AngleAxis(-80.0f, Vector3.up) * evt.GetInaccuracy(0.0f) * evt.direction;
                        childEvt.currentChain.Trigger(childEvt);
                        break;
                }
            }
            m_blockRetrigger = false;
        }
    }
    public override string GetName()
    {
        return Shape.ToString();
    }

    public override string GetDescription()
    {
        return "Splits all projectiles in this chain into a new pattern. Also applies to all chains below.";
    }
    public override Sprite GetSprite()
    {
        switch(Shape)
        {
            default:
            case SplitShape.Diagonal2:
                return SpellEditorUI.Instance.Split2Sprite;
            case SplitShape.Diagonal4:
                return SpellEditorUI.Instance.Split4Sprite;
            case SplitShape.Side:
                return SpellEditorUI.Instance.SplitSidewaysSprite;
        }
    }
}
