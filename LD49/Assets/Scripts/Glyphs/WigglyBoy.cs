﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class WigglyBoy : Glyph, IGlyphMovementModifier
{
    public enum WiggleMode
    {
        SinWave
    }

    public WiggleMode Mode { get; set; }

    public void Tick(GlyphBehaviour instance)
    {
        Rigidbody body = instance.GetComponent<Rigidbody>();

        float extraSpeed = Time.fixedDeltaTime * 50.0f;
        Vector3 vector;

        switch(Mode)
        {
            default:
            case WiggleMode.SinWave:
                vector = new Vector3(-instance.SourceEvent.direction.z, 0.0f, instance.SourceEvent.direction.x);
                break;

        }

        body.velocity += Mathf.Cos(instance.LifeTime * 5.0f) * vector.normalized * extraSpeed;
    }

    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        if(source == TriggerSource.Normal)
            evt.movementModifiers.Add(this);
    }

    public override string GetName()
    {
        return "Wiggle";
    }

    public override string GetDescription()
    {
        return "Makes projectiles in this chain and all chains below wiggle.";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.WigglyBoySprite;
    }
}
