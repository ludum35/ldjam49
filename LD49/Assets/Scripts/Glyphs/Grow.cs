﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Grow : Glyph
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        evt.SizeMod *= 1.2f * evt.GetSizeMod();
    }

    public override string GetName()
    {
        return "Grow";
    }
    public override string GetDescription()
    {
        return "Grows the spell bigger in this chain and every chain below.";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.GrowSprite;
    }
}
