﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class FrostBolt : Glyph, IGlyphFireTrigger, IGlyphHitTrigger, IGlyphHitWallTrigger
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        float size = evt.GetSizeMod();
        GameObject frostBolt = SpawnPool.Instance.Instantiate(evt.tree.m_frostboltPrefab);
        frostBolt.transform.position = evt.origin + evt.direction * Mathf.Max(0.0f, size - 1.0f);
        frostBolt.transform.forward = evt.direction;
        frostBolt.transform.localScale = Vector3.one * size;

        frostBolt.GetComponent<Rigidbody>().velocity = evt.GetInaccuracy(3.0f) * evt.direction * 
            (evt.tree.m_frostboltSpeed * evt.SpeedMod);

        GlyphBehaviour glyphBehaviour = frostBolt.GetComponent<GlyphBehaviour>();
        glyphBehaviour.IntializeGlyph(this, evt);
    }

    public override string GetName()
    {
        return "Frost bolt";
    }
    public override string GetDescription()
    {
        return "Makes the spell bounce when hitting a wall. +1 bounce per glyph.";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.FrostBoltSprite;
    }
}
