﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class MagicBolt : Glyph, IGlyphFireTrigger, IGlyphHitTrigger, IGlyphHitWallTrigger
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        float size = evt.GetSizeMod();

        GameObject magicBolt = SpawnPool.Instance.Instantiate(evt.tree.m_magicBoltPrefab);
        magicBolt.transform.position = evt.origin + evt.direction * Mathf.Max(size - 1.0f);
        magicBolt.transform.forward = evt.direction;
        magicBolt.transform.localScale = Vector3.one * size;

        magicBolt.GetComponent<Rigidbody>().velocity = 
            evt.GetInaccuracy(2.0f) * evt.direction * 
            (evt.tree.m_magicBoltSpeed * evt.GetSpeedMod());

        GlyphBehaviour glyphBehaviour = magicBolt.GetComponent<GlyphBehaviour>();
        glyphBehaviour.IntializeGlyph(this, evt);
    }

    public override string GetName()
    {
        return "Magic bolt";
    }
    public override string GetDescription()
    {
        return "";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.MagicBoltSprite;
    }
}
