﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class OnHitChain : Glyph, IGlyphOnHitListener, IGlyphOnHitWallListener
{
    public GlyphChain Chain { get; set; }

    public OnHitChain()
    {
        Chain = new GlyphChain();
    }

    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        if(source == TriggerSource.HitWall || source == TriggerSource.Hit)
        {
            if(evt.lastCollision != null)
            { 
                if(evt.charges > 0)
                { 
                    // Create clone of event and trigger all
                    CastSpellEvent childEvt = evt.Copy();

                    childEvt.direction = evt.lastCollisionSurfaceNormal;
                    childEvt.origin = evt.lastCollision.contacts[0].point + evt.lastCollisionSurfaceNormal * 0.1f;

                    childEvt.currentChain = Chain;
                    Chain.Trigger(childEvt);
                }
            }
        }
    }

    public override string GetName()
    {
        return "On hit";
    }

    public override string GetDescription()
    {
        return "Triggers a new chain of glyphs when any projectile in this chain hits a target or wall.";
    }

    public override GlyphChain GetSubChain()
    {
        return Chain;
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.OnHitChainSprite;
    }
}
