﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class SlowDown : Glyph
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        evt.SpeedMod *= 0.75f;
    }
    public override string GetName()
    {
        return "Slower";
    }

    public override string GetDescription()
    {
        return "Slows down projectiles on this chain and all chains below.";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.SlowDownSprite;
    }
}
