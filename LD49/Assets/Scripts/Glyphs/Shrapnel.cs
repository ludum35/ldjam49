﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Shrapnel : Glyph, IGlyphFireTrigger, IGlyphHitWallTrigger, IGlyphHitTrigger
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        if(source == TriggerSource.Normal)
        {
            int layer = 1 << LayerMask.NameToLayer("Default");

            for(int i = 0; i < 8; ++i)
            {
                Vector3 direction;

                if(evt.lastCollision != null)
                    direction = Quaternion.AngleAxis(UnityEngine.Random.Range(-90.0f, 90.0f), Vector3.up) * evt.lastCollisionSurfaceNormal;
                else
                {
                    float r = UnityEngine.Random.Range(0.0f, Mathf.PI * 2.0f);
                    direction = new Vector3(Mathf.Cos(r), UnityEngine.Random.Range(-0.15f, -0.3f), Mathf.Sin(r));
                    direction.Normalize();
                }

                GameObject shard = SpawnPool.Instance.Instantiate(evt.tree.m_shrapnelFragmentPrefab, evt.origin, Quaternion.identity, null);
                GlyphBehaviour behaviour = shard.GetComponent<GlyphBehaviour>();
                behaviour.IntializeGlyph(this, evt);

                shard.GetComponent<Rigidbody>().velocity = direction * 75.0f;
            }
        }
    }
    public override string GetName()
    {
        return "Shrapnel";
    }
    public override string GetDescription()
    {
        return "8 random shards of sharpnell flying off in all directions.";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.ShrapnelSprite;
    }
}
