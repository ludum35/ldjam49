﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Trail : Glyph, IGlyphMovementModifier, IDoNotInherit
{
    public GlyphChain Chain { get; set; }

    public Trail()
    {
        Chain = new GlyphChain();
    }

    public void Tick(GlyphBehaviour instance)
    {
        instance.TrailTick += instance.LastVelocity.magnitude * Time.fixedDeltaTime;
        if(instance.TrailTick > 1.0f)
        {
            if(instance.TraveledDistance > 1.0f)
            { 
                if(instance.SourceEvent.charges > 0)
                { 
                    CastSpellEvent evt = instance.SourceEvent.Copy();
                    evt.currentChain = Chain;
                    evt.origin = instance.transform.position;
                    evt.direction = (Quaternion.AngleAxis(UnityEngine.Random.Range(45.0f, 315.0f), Vector3.up) * instance.LastVelocity).normalized;
                    evt.SpeedMod = 1.0f;
                    Chain.Trigger(evt);
                }
            }


            instance.TrailTick -= 1.0f;
        }
    }

    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        if(source == TriggerSource.Normal)
            evt.movementModifiers.Add(this);
    }
    public override string GetName()
    {
        return "Leave trail";
    }

    public override GlyphChain GetSubChain()
    {
        return Chain;
    }
    public override string GetDescription()
    {
        return "Triggers a new chain of glyphs every so often along a projectiles path.";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.TrailSprite;
    }
}
