﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public enum SprayType
{
    Acid,
    Oil,
    Water,
    Flames,
    IceBlast,
    LightningBlast
}

public class Spray : Glyph
{
    public SprayType Type { get; set; }

    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        GameObject spray = null;

        switch(Type)
        {
            case SprayType.Acid:
                spray = SpawnPool.Instance.Instantiate(evt.tree.m_acidSprayPrefab);
                break;
            case SprayType.Oil:
                spray = SpawnPool.Instance.Instantiate(evt.tree.m_oilSprayPrefab);
                break;
            case SprayType.Water:
                spray = SpawnPool.Instance.Instantiate(evt.tree.m_waterSprayPrefab);
                break;
            case SprayType.Flames:
                spray = SpawnPool.Instance.Instantiate(evt.tree.m_flameSprayPrefab);
                break;
            case SprayType.IceBlast:
                spray = SpawnPool.Instance.Instantiate(evt.tree.m_iceblastPrefab);
                break;
            case SprayType.LightningBlast:
                spray = SpawnPool.Instance.Instantiate(evt.tree.m_lightningPrefab);
                break;
        }

        spray.transform.position = evt.origin;
        spray.transform.forward = evt.direction;
        spray.transform.localScale = Vector3.one * evt.GetSizeMod();

        GlyphBehaviour glyphBehaviour = spray.GetComponent<GlyphBehaviour>();
        glyphBehaviour.IntializeGlyph(this, evt);
    }
    public override string GetName()
    {
        switch(Type)
        {
            case SprayType.Acid: return "Acid spray";
            case SprayType.Oil: return "Oil spray";
            case SprayType.Water: return "Water spray";
            case SprayType.Flames: return "Flames";
            case SprayType.IceBlast: return "Ice blast";
            case SprayType.LightningBlast: return "Lightning blast";
        }

        return $"Spray {Type.ToString()}";
    }


    public override string GetDescription()
    {
        switch(Type)
        {
            case SprayType.Acid:
                return "A spray of acid. Deals damage over time. Removed with fire or water.";
            case SprayType.Oil:
                return "A spray of oil. Makes enemies more ignitable.";            
            case SprayType.Water:
                return "A spray of water. Douses flames. Removes acid. Removes oil.";
            case SprayType.Flames:
                return "A spray of flames. Sets non wet targets on fire.";
            case SprayType.IceBlast:
                return "A blast of ice. Freezes wet enemies. Slows enemies down.";
            case SprayType.LightningBlast:
                return "A blast of lightning. Deals high damage. Even higher damage to wet targets.";
        }

        return "";
    }

    public override Sprite GetSprite()
    {
        switch(Type)
        {
            default:
            case SprayType.Acid: return SpellEditorUI.Instance.SprayAcidSprite;
            case SprayType.Oil: return SpellEditorUI.Instance.SprayOilSprite;
            case SprayType.Water: return SpellEditorUI.Instance.SprayWaterSprite;
            case SprayType.Flames: return SpellEditorUI.Instance.SprayFlamesSprite;
            case SprayType.IceBlast: return SpellEditorUI.Instance.IceBlastSprite;
            case SprayType.LightningBlast: return SpellEditorUI.Instance.LightningBlastSprite;
        }
    }
}
