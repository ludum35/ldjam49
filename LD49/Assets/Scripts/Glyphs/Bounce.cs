﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Bounce : Glyph, IGlyphOnHitWallListener, ITriggerOnce
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        if(source == TriggerSource.Normal)
        {
            evt.BouncesLeft++;
        }
        else if(source == TriggerSource.HitWall)
        {
            if(evt.lastCollision.contactCount > 0 && evt.BouncesLeft > 0)
            {
                evt.BouncesLeft--;

                CastSpellEvent childEvent = evt.Copy();

                ContactPoint contact = evt.lastCollision.contacts[0];
                childEvent.direction = Vector3.Reflect(evt.thisInstance.LastVelocity.normalized, evt.lastCollisionSurfaceNormal);
                childEvent.origin = contact.point + childEvent.direction * 0.25f * evt.SizeMod;

                childEvent.currentChain.Trigger(childEvent);
            }
        }
    }

    public override string GetName()
    {
        return "Bounce";
    }

    public override string GetDescription()
    {
        return "Makes the spell bounce when hitting a wall. +1 bounce per glyph.";
    }

    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.BounceSprite;
    }
}
