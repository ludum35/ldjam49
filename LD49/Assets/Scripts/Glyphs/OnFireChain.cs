﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class OnFireChain : Glyph, IGlyphOnFireListener
{
    public GlyphChain Chain { get; set; }

    public OnFireChain()
    {
        Chain = new GlyphChain();
    }

    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        if(source == TriggerSource.Fire)
        {
            if(evt.charges > 1)
            { 
                // Create clone of event and trigger all
                CastSpellEvent childEvt = evt.Copy();
                childEvt.currentChain = Chain;
                Chain.Trigger(childEvt);
            }
        }
    }

    public override string GetName()
    {
        return "On fire";
    }
    public override string GetDescription()
    {
        return "Triggers a new chain of glyphs when any glyph in the current chain fires.";
    }
    public override GlyphChain GetSubChain()
    {
        return Chain;
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.OnFireChainSprite;
    }
}
