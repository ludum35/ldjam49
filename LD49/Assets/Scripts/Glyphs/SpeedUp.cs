﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class SpeedUp : Glyph
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        evt.SpeedMod *= 1.25f;
    }
    public override string GetName()
    {
        return "Faster";
    }

    public override string GetDescription()
    {
        return "Speeds up projectiles in this chain and all chains below.";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.SpeedUpSprite;
    }
}
