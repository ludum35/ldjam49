﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Gravity : Glyph, IGlyphMovementModifier
{
    public enum GravityMode
    {
        Straight,
        Backwards,
        Left,
        Right
    }

    public GravityMode Mode { get; set; }

    public void Tick(GlyphBehaviour instance)
    {
        Rigidbody body = instance.GetComponent<Rigidbody>();

        float extraSpeed = Time.fixedDeltaTime * 10.0f;
        Vector3 vector;

        switch(Mode)
        {
            default:
            case GravityMode.Straight:
                vector = instance.SourceEvent.direction;
                break;
            case GravityMode.Left:
                vector = new Vector3(-body.velocity.z, 0.0f, body.velocity.x);
                break;
            case GravityMode.Right:
                vector = new Vector3(body.velocity.z, 0.0f, -body.velocity.x);
                break;
            case GravityMode.Backwards:
                vector = -instance.SourceEvent.direction;
                break;

        }

        body.velocity += vector.normalized * extraSpeed;
    }

    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        if(source == TriggerSource.Normal)
            evt.movementModifiers.Add(this);
    }

    public override string GetName()
    {
        return "Gravity";
    }
    public override string GetDescription()
    {
        return $"Makes the spell veer {Mode}";
    }

    public override Sprite GetSprite()
    {
        switch(Mode)
        {
            default:
            case GravityMode.Straight:
                return SpellEditorUI.Instance.GravityForward;
            case GravityMode.Backwards:
                return SpellEditorUI.Instance.GravityBackwards;
            case GravityMode.Left:
                return SpellEditorUI.Instance.GravityLeft;
            case GravityMode.Right:
                return SpellEditorUI.Instance.GravityRight;
        }
    }
}
