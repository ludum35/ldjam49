﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class MoreDamage : Glyph
{
    public override void Trigger(CastSpellEvent evt, TriggerSource source)
    {
        evt.DamageMod *= 1.1f + UnityEngine.Random.Range(1.0f, Mathf.Log10(1.0f + evt.instability));
    }

    public override string GetName()
    {
        return "Intensify";
    }
    public override string GetDescription()
    {
        return "Increases all damage of glyphs in this chain and below.";
    }
    public override Sprite GetSprite()
    {
        return SpellEditorUI.Instance.MoreDamageSprite;
    }
}
