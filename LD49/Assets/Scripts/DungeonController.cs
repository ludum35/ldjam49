using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

[DefaultExecutionOrder(-10)]
public class DungeonController : MonoBehaviour
{
    public List<GameObject> m_levels;
    public List<GameObject> m_enemies;
    public bool m_activate = true;

    public static int m_roomsLeft;
    public static DungeonController Instance { get; private set; }
    public static bool IsInDungeon = false;

    public bool CanLeave { get { return m_enemies.Count == 0; } }

    public UnityEvent OnGameOverUnity;

    private void Awake()
    {
        m_roomsLeft--;

        Instance = this;

        if(m_activate)
            m_levels[UnityEngine.Random.Range(0, m_levels.Count)].SetActive(true);
    }

    public void ExitLevel()
    {
        IsInDungeon = true;

        if (m_roomsLeft <= 0)
        {
            IsInDungeon = false;
            SceneManager.LoadScene("Town");
        }
        else
        {
            IsInDungeon = true;
            SceneManager.LoadScene("GameScene");
        }
    }

    public void OnGameOver()
    {
        Debug.LogError("You lose.");

        OnGameOverUnity?.Invoke();
    }
}