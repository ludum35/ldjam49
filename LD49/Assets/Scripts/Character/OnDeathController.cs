using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDeathController : MonoBehaviour
{
    public GameObject DeathFX;
    public GameObject Soul;
    public bool SpawnDeathFXAfterTime = false;
    public float DestroyAfterTime = -1f;

    [SerializeField]
    [Header("Readonly pls")]
    private bool Dead = false;

    public void Awake()
    {
        if (TryGetComponent<Health>(out var _hp))
        {
            _hp.OnDeath += OnDeath;
        }
    }

    public void OnDeath()
    {
        if (Dead) { return; }
        Dead = true;

        if (!SpawnDeathFXAfterTime)
        {
            if (DeathFX != null)
            {
                Instantiate(DeathFX, transform.position, transform.rotation, null);
            }
        }

        var ai = GetComponent<I_AI>();
        if (ai != null) { ai.Stop(); }

        if (DestroyAfterTime >= 0f)
        {
            this.InvokeDelayed(DestroyAfterTime, () => Destroy(gameObject));
        }

        if(UnityEngine.Random.value < 0.5f)
            Instantiate(Soul, transform.position + new Vector3(0.0f, 0.0f, -1.0f), Quaternion.identity);
    }

    private void OnDestroy()
    {
        if (SpawnDeathFXAfterTime)
        {
            if (DeathFX != null)
            {
                Instantiate(DeathFX, transform.position, transform.rotation, null);
            }
        }
    }
}
