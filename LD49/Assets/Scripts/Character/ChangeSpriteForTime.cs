using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSpriteForTime : MonoBehaviour
{
    public float DamageTime = .2f;
    public Sprite OriginalSprite;
    public Sprite DamageSprite;

    public SpriteRenderer rend;
    public bool m_isActive = false;
    public void Start()
    {
        rend = GetComponent<SpriteRenderer>();

        OriginalSprite = rend.sprite;
    }

    public void LateUpdate()
    {
        if (m_isActive)
        {
            rend.sprite = DamageSprite;
        }
    }

    public void DoSwapForTime()
    {
        m_isActive = true;
        this.InvokeDelayed(DamageTime, () => m_isActive = false);
    }
}
