using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public AISpawnerWeightsScriptable AISpawnerWeights;

    public void SpawnForPoints(float nrPoints)
    {
        var pointsLeft = nrPoints;

        for (int i = 0; i < 100; i++)
        {
            if (pointsLeft > 0)
            {
                var creep = AISpawnerWeights.GetPrefabForPoints(pointsLeft, out float usedPoints);
                if (creep == null && usedPoints == 0f) { return; }

                Instantiate(creep, transform.position, Quaternion.identity, null);

                pointsLeft -= usedPoints;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, 1f);
    }
}
