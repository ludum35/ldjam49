using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GlyphBehaviour : MonoBehaviour
{
    public float m_damageOnHit = 1.0f;
    public GameObject m_spawnOnHit;
    public DamageType m_onHitDamageType;
    public StatusEffectType m_statusEffectType;
    public bool m_destroyOnCollision = true;
    public float m_maxTravelDistance = -1.0f;

    float m_velocityAtStart;
    HashSet<Collider> m_damaged = new HashSet<Collider>();
    public float TraveledDistance { get; set; }

    public Vector3 LastVelocity { get; set; }
    public float LifeTime { get; private set; }
    public Glyph Source { get; private set; }
    public CastSpellEvent SourceEvent { get; private set; }
    public float TrailTick { get; set; }


    void OnEnable()
    {
        TrailTick = 0;
        LifeTime = 0;
        LastVelocity = Vector3.zero;
        m_velocityAtStart = 0.0f;
        TraveledDistance = 0.0f;
        Source = null;
        SourceEvent = null;
        m_damaged.Clear();

        TrailRenderer[] trs = GetComponentsInChildren<TrailRenderer>();
        trs.ToList().ForEach(tr => tr.enabled = false);
    }


    IEnumerator WaitForSeparation(Collider a, Collider b)
    {
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();

        Physics.IgnoreCollision(a, b, false);
    }


    public virtual void IntializeGlyph(Glyph source, CastSpellEvent evt)
    {
        Source = source;
        SourceEvent = evt;

        if(evt.lastCollision != null)
        {
            Collider[] colliders = GetComponentsInChildren<Collider>();
            foreach(Collider c in colliders)
            {
                Physics.IgnoreCollision(c, evt.lastCollision.collider);
                StartCoroutine(WaitForSeparation(c, evt.lastCollision.collider));
            }
        }

        if(GetComponent<Rigidbody>())
            m_velocityAtStart = GetComponent<Rigidbody>().velocity.magnitude;




        TrailRenderer[] trs = GetComponentsInChildren<TrailRenderer>();
        trs.ToList().ForEach(tr =>
        {
            tr.enabled = true;
            tr.Clear();
        });
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!m_damaged.Contains(other))
        { 
            m_damaged.Add(other);
            Health health = other.gameObject.GetComponent<Health>();
            if(health)
            {
                if(m_damageOnHit > 0.0f)
                {
                    health.Change(-m_damageOnHit * SourceEvent.GetDamageMod(), m_onHitDamageType);
                }
                health.ApplyStatus(m_statusEffectType);

                if(Source is IGlyphHitTrigger)
                {
                    Source.Parent.RaiseHitEvent(SourceEvent);
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.GetType() == typeof(CharacterController)) { return; }

        Health health = collision.gameObject.GetComponent<Health>();
        if(health)
        {
            if(m_damageOnHit > 0.0f)
            {
                health.Change(-m_damageOnHit * SourceEvent.GetDamageMod(), m_onHitDamageType);
            }
            health.ApplyStatus(m_statusEffectType);

            if(m_spawnOnHit != null && collision.contactCount > 0)
                Instantiate(m_spawnOnHit, collision.contacts[0].point, Quaternion.LookRotation(collision.contacts[0].normal, Vector3.up));


            if(Source is IGlyphHitTrigger)
            {
                SourceEvent.lastCollision = collision;
                SourceEvent.lastCollisionSurfaceNormal = collision.contacts[0].normal;
                Source.Parent.RaiseHitEvent(SourceEvent);
            }
        }
        else
        {
            if(Source is IGlyphHitWallTrigger)
            {
                SourceEvent.SpeedMod += Mathf.Max(0.0f, LastVelocity.magnitude - m_velocityAtStart);

                RaycastHit info;
                Ray ray = new Ray(collision.contacts[0].point + collision.contacts[0].normal, -collision.contacts[0].normal);
                if(collision.collider.Raycast(ray, out info, 2.0f))
                {
                    SourceEvent.lastCollision = collision;
                    SourceEvent.lastCollisionSurfaceNormal = info.normal;
                    Source.Parent.RaiseWallHitEvent(SourceEvent);
                }
            }
        }

        if(m_destroyOnCollision)
        {
            TrailRenderer[] trs = GetComponentsInChildren<TrailRenderer>();
            trs.ToList().ForEach(tr =>
            {
                Instantiate(tr.gameObject, tr.transform.parent);

                tr.gameObject.transform.SetParent(null);
                Destroy(tr.gameObject, 0.5f);
            });

            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        foreach(IGlyphMovementModifier mod in SourceEvent?.movementModifiers)
        {
            mod.Tick(this);
        }

        SourceEvent.thisInstance = this;

        if(GetComponent<Rigidbody>())
        {
            LastVelocity = GetComponent<Rigidbody>().velocity;
            TraveledDistance += LastVelocity.magnitude * Time.fixedDeltaTime;
        }

        LifeTime += Time.deltaTime;
        if(LifeTime >= 5.0f || (m_maxTravelDistance > 0.0f && TraveledDistance >= m_maxTravelDistance))
            SpawnPool.Instance.Destroy(gameObject);
    }
}
