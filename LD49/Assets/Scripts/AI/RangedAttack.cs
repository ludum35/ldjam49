using Ludum.AI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttack : AttackBehaviour
{
    public float Speed;

    public override bool Attack(Vector3 targetPos, Action<GameObject> FX)
    {
        if (FX == null) { FX = (x) => { }; }

        FX += DoAttackProjectile;

        if (!base.Attack(targetPos, FX)) { return false; }

        return true;
    }

    private void DoAttackProjectile(GameObject fx)
    {
        var missile = fx.GetComponent<Projectile>();
        missile.m_damageOnHit = AttackDamage;
        missile.SetProjectileParams(missile.transform.forward, Speed);
    }
}
