using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMeleeAttack
{
    float AttackDistance { get; }

    float AttackDamage { get; }

    bool Attack(Vector3 towards);
}
