using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitHandler : MonoBehaviour
{
    public AttackBehaviour Origin;

    private void OnTriggerEnter(Collider other)
    {
        OnWhatever(other.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        OnWhatever(collision.gameObject);
    }

    void OnWhatever(GameObject other)
    {
        if (Origin != null) { Origin.OnAttackHitExternal(other); }
    }
}
