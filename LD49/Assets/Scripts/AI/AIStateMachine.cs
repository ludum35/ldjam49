using MonsterLove.StateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIStateMachine : MonoBehaviour
{
    public enum States
    {
        Init,
        Play,
        Win,
        Lose
    }

    StateMachine<States> fsm;

    private void Awake()
    {
        fsm = new StateMachine<States>(this);
    }

    private void Update()
    {
        fsm.Driver.Update?.Invoke();
    }
}
