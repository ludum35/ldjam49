using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIStatusEffectsController : MonoBehaviour
{
    public AIStatusEffectScriptable AIStatusEffectsScriptable;

    public float Speed = 3.5f;

    public StatusEffects StatusEffects;
    public NavMeshAgent agent;

    public bool TakeSpeedAtStartUp = true;

    public void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        if (TakeSpeedAtStartUp)
        {
            Speed = agent.speed;
        }
        StatusEffects = GetComponent<StatusEffects>();
        StatusEffects.OnTypeAdded += EvaluateStatusEffects;
    }

    private void Update()
    {

    }

    private void EvaluateStatusEffects(StatusEffectType obj)
    {
        float worstSpeedMod = 1f;

        foreach (var k in StatusEffects.GetStatusEffects())
        {
            var mod = AIStatusEffectsScriptable.SpeedMods.FirstOrDefault((x) => x.Condition == k);
            if (mod != null)
            {
                if (mod.Modifier < worstSpeedMod) { worstSpeedMod = mod.Modifier; }
            }
        }

        agent.speed = Speed * worstSpeedMod;
    }
}
