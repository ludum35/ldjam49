using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum AIMeleeStates
{
    MoveTowardsTarget,
    Dodge,
    MoveAway,
    Attack
}

/// <summary>
/// Generic melee kreep
/// </summary>
public class AIMeleeBehaviour : StateDrivenAI<AIMeleeStates>
{
    public NavMeshAgent agent;
    public Transform Target;

    public AIMoveTowardsTarget moveBehaviour;

    public Behaviour MeleeBehaviour;
    public IMeleeAttack MeleeAttack;

    private Vector3 m_currentTarget;

    private Vector3 m_attackTarget;

    private float m_attackPosUpdate = 0f;

    protected override void Awake()
    {
        base.Awake();

        if (MeleeBehaviour == null) { MeleeBehaviour = GetComponent<AttackBehaviour>(); }

        this.MeleeAttack = MeleeBehaviour as IMeleeAttack;

        moveBehaviour = new AIMoveTowardsTarget(agent, transform.position);

        Target = FindObjectOfType<PlayerController>().transform; // always player for now.

        agent = GetComponentInChildren<NavMeshAgent>();

        StateMachine.ChangeState(AIMeleeStates.MoveTowardsTarget);
    }

    protected override void Update()
    {
        base.Update();

        if (Stopped) { agent.isStopped = true; }
    }

    protected override void UpdateAI()
    {
        //moveBehaviour?.Update();
    }

    void MoveTowardsTarget_Enter()
    {

    }

    public Vector3 GetDirToPlayer()
    {
        var dir = (Target.transform.position - agent.transform.position).x0z();
        return dir;
    }

    protected virtual void MoveTowardsTarget_Update()
    {
        if (moveBehaviour == null) { return; }

        var dir = GetDirToPlayer().normalized;

        if (dir == Vector3.zero) { dir = Vector3.right; }

        moveBehaviour.MoveTowards(agent, Target.transform.position - (dir * .6f));

        if (dir.magnitude < MeleeAttack.AttackDistance)
        {
            //moveBehaviour.Stop();
            StateMachine.ChangeState(AIMeleeStates.Attack);
        }
    }

    protected virtual void Attack_Update()
    {
        var dir = (Target.transform.position - agent.transform.position).x0z().normalized;

        if (dir == Vector3.zero) { dir = Vector3.right; }

        var perpendicular = new Vector3(-dir.z, 0f, dir.x);
        var range = Random.Range(-6f, 6f);


        if (Vector3.Distance(transform.position, Target.transform.position) < MeleeAttack.AttackDistance)
        {
            if (Time.time - m_attackPosUpdate > 1f)
            {
                m_attackPosUpdate = Time.time;
                m_attackTarget = perpendicular * range;
            }

            var moveTarget = Target.transform.position + m_attackTarget;

            Debug.DrawLine(transform.position, moveTarget);

            Debug.DrawLine(transform.position, Target.transform.position, Color.blue, .2f);

            moveBehaviour.MoveTowards(agent, moveTarget);

            if (MeleeBehaviour is IMeleeAttack attackBehaviour)
            {
                attackBehaviour.Attack(Target.transform.position);
                return;
            }
        }

        if (Vector3.Distance(transform.position, Target.transform.position) > MeleeAttack.AttackDistance)
        {
            StateMachine.ChangeState(AIMeleeStates.MoveTowardsTarget);
        }
    }
}
