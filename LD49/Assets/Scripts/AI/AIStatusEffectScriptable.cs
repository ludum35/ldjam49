using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AIStatusEffectScriptable : ScriptableObject
{
    [Serializable]
    public class SpeedMod
    {
        public StatusEffectCondition Condition;
        public float Modifier;

        public SpeedMod(StatusEffectCondition condition, float modifier)
        {
            Condition = condition;
            Modifier = modifier;
        }
    }

    private static List<SpeedMod> GetDefaultSpeedModifiers()
    {
        return new List<SpeedMod>{
            new SpeedMod(StatusEffectCondition.IsWet, .8f),
            new SpeedMod(StatusEffectCondition.IsFrozen, 0f),
            new SpeedMod(StatusEffectCondition.IsOiled, .7f),
            new SpeedMod(StatusEffectCondition.IsAcid, .8f)
        };
    }

    public List<SpeedMod> SpeedMods = GetDefaultSpeedModifiers();
}
