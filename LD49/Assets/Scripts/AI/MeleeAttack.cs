using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackBehaviour : MonoBehaviour
{
    public float AttackDistance => m_AttackDistance;
    public float AttackDamage => m_AttackDMG;

    public float m_AttackDistance;

    public DamageType m_DamageType = DamageType.Kinetic;

    public float m_AttackDMG;
    public float m_AttackDelay;

    public float HitCooldown = 2f;

    public float FXDistance = .5f;

    public GameObject AttackFX;

    public Vector3 spawnOffset = new Vector3(0f, .2f, 0f);

    protected bool canHit = false;
    protected float nextHit = 0f;

    public bool CanHitSelf = false;

    public event Action OnAttackStarted;
    public UnityEvent OnAttackStartedUnity;

    public virtual void Start()
    {
        OnAttackStarted += () => OnAttackStartedUnity?.Invoke();
    }

    public virtual void Update()
    {
        if (Time.time > nextHit) { canHit = true; }
    }

    public virtual bool Attack(Vector3 targetPos, Action<GameObject> FX)
    {
        if (!canHit) { return false; }

        canHit = false;
        nextHit = Time.time + HitCooldown;

        OnAttackStarted?.Invoke();

        this.InvokeDelayed(m_AttackDelay, () => FX?.Invoke(SpawnAttack(targetPos)));
        //FX += (x) => SpawnAttack(targetPos);

        return true;
    }

    protected virtual GameObject SpawnAttack(Vector3 targetPos)
    {
        if (AttackFX == null) { Debug.Log("no attack fx"); return null; }

        var dir = targetPos - transform.position;

        dir = dir.x0z();

        var attackSpawn = transform.position + (dir.normalized * FXDistance) + spawnOffset;

        var attack = Instantiate(AttackFX, attackSpawn, Quaternion.identity, null);

        attack.transform.forward = dir;

        return attack;
    }

    public virtual void OnAttackHitExternal(GameObject gameObject)
    {

    }
}


public class MeleeAttack : AttackBehaviour, IMeleeAttack
{
    public bool DoRayCast = true;

    public override void Update()
    {
        base.Update();
    }

    public bool Attack(Vector3 targetPos) // interface.
    {
        return Attack(targetPos, null);
    }

    public override bool Attack(Vector3 targetPos, Action<GameObject> FX)
    {
        if (FX == null) { FX = (x) => { }; }
        var target = targetPos;

        Action<GameObject> OnSpawn = (x) =>
        {
            if (DoRayCast)
            {
                OnTargetHitRaycast(target);
            }
            else
            {
                DoFXBoxHit(x);
            }
        };

        FX += OnSpawn.Invoke;

        if (!base.Attack(target, FX)) { return false; }

        return true;
    }

    private void DoFXBoxHit(GameObject fX)
    {
        var hit = fX.AddComponent<AttackHitHandler>();
        hit.Origin = this;
    }

    public override void OnAttackHitExternal(GameObject gameObject)
    {
        if (gameObject == null) { return; }
        if (gameObject.TryGetComponent<Health>(out var _hp))
        {
            if (!CanHitSelf)
            {
                if (_hp == GetComponent<Health>()) { return; }
            }
            _hp.Change(-AttackDamage, m_DamageType);
        }
    }


    protected virtual bool OnTargetHitRaycast(Vector3 targetPos)
    {
        var dir = targetPos - transform.position;

        var ray = new Ray(transform.position, dir);

        var rayCast = Physics.RaycastAll(ray, AttackDistance);

        Debug.DrawRay(ray.origin, ray.direction * AttackDistance, Color.red, 2f);

        for (int i = 0; i < rayCast.Length; i++)
        {
            OnStrikeHit(rayCast[i]);
            return true;
        }

        return false;
    }

    public void OnStrikeHit(RaycastHit hit)
    {
        if (hit.transform == null) { return; }
        OnAttackHitExternal(hit.transform.gameObject);
    }
}
