using MonsterLove.StateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface I_AI // death listener?
{
    void Stop();
}

public abstract class StateDrivenAI<T> : MonoBehaviour, I_AI where T : struct, IConvertible, IComparable
{
    protected StateMachine<T> StateMachine;

    public bool Stopped = false;

    protected virtual void Awake()
    {
        StateMachine = new StateMachine<T>(this);
    }

    public void Stop()
    {
        Stopped = true;
    }

    protected virtual void Update()
    {
        if (Stopped) { return; }

        StateMachine.Driver.Update?.Invoke();

        UpdateAI();
    }

    protected abstract void UpdateAI();
}
