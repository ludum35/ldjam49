using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class AISpawnerWeightsScriptable : ScriptableObject
{
    [Serializable]
    public class EnemyCosts
    {
        public GameObject AIPrefab;
        public float Cost;
    }

    [SerializeField]
    public List<EnemyCosts> enemyCosts = new List<EnemyCosts>();

    public GameObject GetPrefabForPoints(float maxCost, out float usedPoints)
    {
        usedPoints = 0;

        var viable = enemyCosts.Where((x) => x.Cost <= maxCost).ToList();

        if (viable.Count == 0) { return null; }

        int index = Random.Range(0, viable.Count);
        var randomViable = viable[index];

        usedPoints = randomViable.Cost;

        return randomViable.AIPrefab;
    }
}
