using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public enum WallEyeStatesEnum
{
    Sleep,
    Fire
}

public class AIWallEyeBehaviour : StateDrivenAI<WallEyeStatesEnum>
{
    public Transform Target;
    public NavMeshAgent agent;

    public RangedAttack AttackBehaviour;

    public float lastShoot = 0f;

    public float ReadyToFireTime = 0f;

    public float shootTimeLength = 2f;

    public SpriteRenderer ClosedEye;
    public GameObject LaserFX;

    public bool EyeOpen;
    public float OpenDirAngle = .8f;
    public float StartShootingDir = .5f;

    protected override void Awake()
    {
        base.Awake();

        Target = FindObjectOfType<PlayerController>().transform; // always player for now.

        agent = GetComponent<NavMeshAgent>();

        if (AttackBehaviour == null) { AttackBehaviour = GetComponentInChildren<RangedAttack>(); }

        StateMachine.ChangeState(WallEyeStatesEnum.Sleep);
    }

    public Vector3 GetDirToPlayer()
    {
        var dir = (Target.transform.position - agent.transform.position).x0z();
        return dir;
    }

    public Vector3 GetStraightForwardsDirection()
    {
        return transform.right;
    }

    public void Sleep_Update()
    {
        var playerDir = GetDirToPlayer().normalized.x0z();
        var facing = GetStraightForwardsDirection().normalized.x0z();

        var diff = playerDir - facing;

        CloseEye();

        if (diff.magnitude < StartShootingDir) // wait for player to get more into range.
        {
            StateMachine.ChangeState(WallEyeStatesEnum.Fire);
        }
    }

    private void CloseEye()
    {
        EyeOpen = false;
        CloseLaser();
        ClosedEye.DOFade(1f, .5f);
    }

    private void OpenEye()
    {
        if (EyeOpen) { return; }
        EyeOpen = true;
        ReadyToFireTime = Time.time + .5f;
        ClosedEye.DOFade(0f, .5f);
    }

    public void Fire_Update()
    {
        var playerDir = GetDirToPlayer().normalized.x0z();
        var facing = GetStraightForwardsDirection().normalized.x0z();

        var diff = playerDir - facing;
        bool hitPlayer = false;

        if (diff.magnitude > StartShootingDir) // wait for player to get more into range.
        {
            if (LaserFX == null) // not shooting go sleep.
            {
                CloseLaser();
                StateMachine.ChangeState(WallEyeStatesEnum.Sleep);
            }
        }

        if (diff.magnitude < OpenDirAngle)
        {
            if (!EyeOpen) { OpenEye(); }

            if (ReadyToFireTime > Time.time)
            {
                return;
            }

            if (LaserFX != null) // laser open
            {
                if (lastShoot + shootTimeLength < Time.time) //too long open.
                {
                    CloseLaser();
                    StateMachine.ChangeState(WallEyeStatesEnum.Sleep);
                    return;
                }
                UpdateActiveLaser(out hitPlayer);
            }

            if (!ShootCooldown) { return; } // 

            lastShoot = Time.time;

            UpdateActiveLaser(out hitPlayer); // start new shoot.
        }
        else
        {
            CloseLaser();
        }
    }

    private bool ShootCooldown => !(Time.time - lastShoot < AttackBehaviour.HitCooldown);


    private void CloseLaser()
    {
        if (LaserFX != null)
        {
            LaserFX.transform.DOScale(Vector3.up, .2f).OnComplete(() => Destroy(LaserFX.gameObject));
        }
    }

    public void UpdateActiveLaser(out bool hitPlayer)
    {
        ShootLaser(Target.position, out Vector3 hit, out hitPlayer);

        var dist = hit - transform.position;

        dist = dist.x0z();

        if (LaserFX == null)
        {
            LaserFX = Instantiate(AttackBehaviour.AttackFX);
            LaserFX.transform.localScale = Vector3.zero;
        }

        var middle = transform.position + (dist / 2f);

        Debug.DrawLine(transform.position, hit, Color.red);

        LaserFX.transform.position = middle;

        LaserFX.transform.forward = transform.position - hit;

        float time = .2f;

        if (dist.magnitude < LaserFX.transform.localScale.z)
        {
            time = 0f;
        }

        LaserFX.transform.DOScale(new Vector3(1f, 1f, dist.magnitude), time);

        var scaleDist = LaserFX.transform.localScale.z;

        LaserFX.transform.position = transform.position + ((dist.normalized * scaleDist) / 2f);

    }


    protected virtual bool ShootLaser(Vector3 targetPos, out Vector3 hitPoint, out bool canHitPlayer)
    {
        var ray = new Ray(transform.position, targetPos);

        var dist = AttackBehaviour.AttackDistance;

        var rayCast = Physics.RaycastAll(ray, dist);

        var dir = (targetPos - transform.position).x0z().normalized;

        canHitPlayer = false;

        hitPoint = dir * dist;

        var playerDist = 100f;
        var otherDist = 100f;
        RaycastHit closest = new RaycastHit();

        foreach (var k in rayCast)
        {
            if (k.transform.GetComponent<PlayerController>())
            {
                playerDist = k.distance;
                if (k.distance < otherDist)
                {
                    closest = k;
                }
                continue;
            }
            if (k.distance < otherDist)
            {
                closest = k;
                otherDist = k.distance;
            }
        }

        if (otherDist < 100)
        {
            hitPoint = closest.point;
        } else { canHitPlayer = true; }

        if (otherDist > playerDist) { canHitPlayer = true; }

        if (rayCast.Length > 0) { return true; }

        return false;
    }

    protected override void UpdateAI()
    {
        //throw new System.NotImplementedException();
    }
}

