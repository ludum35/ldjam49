using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public abstract class AIBehaviour
{
    public abstract void Update();

    public event Action BehaviourComplete;

    protected void SignalComplete()
    {
        BehaviourComplete?.Invoke();
    }
}

public class AIMoveTowardsTarget : AIBehaviour
{
    public Vector3 m_target;
    public Vector3 m_currentTarget;
    public NavMeshAgent m_agent;

    public AIMoveTowardsTarget(NavMeshAgent agent, Vector3 target)
    {
        m_agent = agent;
        m_target = target;
    }

    public override void Update()
    {
        if (MoveTowards(m_agent, m_target))
        {
            SignalComplete();
        }
    }

    public virtual bool MoveTowards(NavMeshAgent agent, Vector3 target)
    {
        var targetPos = target;

        if (Vector3.Distance(targetPos, m_currentTarget) < .3f)
        {
            return true;
        }

        m_currentTarget = targetPos;

        agent.SetDestination(targetPos);

        return false;
    }

    public virtual void Stop()
    {
        m_agent.SetDestination(m_agent.transform.position);
    }
}
