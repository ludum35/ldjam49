using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum AIRangedStates
{
    MoveTowardsTarget,
    Dodge,
    MoveAway,
    Attack
}

/// <summary>
/// Generic melee kreep
/// </summary>
public class AIRangedBehaviour : StateDrivenAI<AIRangedStates>
{
    public NavMeshAgent agent;
    public Transform Target;

    public AIMoveTowardsTarget moveBehaviour;

    public AttackBehaviour RangedAttack;

    private Vector3 m_currentTarget;

    private Vector3 m_attackTarget;

    private float m_attackPosUpdate = 0f;

    protected override void Awake()
    {
        base.Awake();

        moveBehaviour = new AIMoveTowardsTarget(agent, transform.position);

        Target = FindObjectOfType<PlayerController>().transform; // always player for now.

        agent = GetComponentInChildren<NavMeshAgent>();

        if (RangedAttack == null) { RangedAttack = GetComponentInChildren<AttackBehaviour>(); }

        StateMachine.ChangeState(AIRangedStates.MoveTowardsTarget);
    }

    protected override void Update()
    {
        base.Update();

        if (Stopped) { agent.isStopped = true; }
    }

    protected override void UpdateAI()
    {
        //moveBehaviour?.Update();
    }

    void MoveTowardsTarget_Enter()
    {

    }

    void MoveTowardsTarget_Update()
    {
        if (moveBehaviour == null) { return; }

        var dir = (Target.transform.position - agent.transform.position).x0z().normalized;

        if (dir == Vector3.zero) { dir = Vector3.right; }

        moveBehaviour.MoveTowards(agent, Target.transform.position - (dir * RangedAttack.m_AttackDistance));

        if (dir.magnitude < RangedAttack.AttackDistance - 1f)
        {
            //moveBehaviour.Stop();
            StateMachine.ChangeState(AIRangedStates.Attack);
        }
    }

    void Attack_Update()
    {
        var dir = (Target.transform.position - agent.transform.position).x0z().normalized;
        
        var shootPos = Target.transform.position - (dir * RangedAttack.m_AttackDistance);

        if (dir == Vector3.zero) { dir = Vector3.right; }

        var perpendicular = new Vector3(-dir.z, 0f, dir.x);
        var range = Random.Range(-6f, 6f);


        if (Vector3.Distance(transform.position, Target.transform.position) < RangedAttack.AttackDistance)
        {
            if (Time.time - m_attackPosUpdate > 1f)
            {
                m_attackPosUpdate = Time.time;
                m_attackTarget = perpendicular * range;
            }

            var moveTarget = shootPos + m_attackTarget;

            Debug.DrawLine(transform.position, moveTarget);

            moveBehaviour.MoveTowards(agent, moveTarget);

            RangedAttack.Attack(Target.transform.position, null); // if timed attack etc.
        }

        if (Vector3.Distance(transform.position, Target.transform.position) > RangedAttack.AttackDistance)
        {
            StateMachine.ChangeState(AIRangedStates.MoveTowardsTarget);
        }
    }
}
