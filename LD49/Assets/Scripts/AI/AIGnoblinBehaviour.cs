using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIGnoblinBehaviour : AIMeleeBehaviour
{
    public float RunAtPlayerDistanceLimit = 3f;
    public float RunAwayMaxDist = 7f;

    protected override void MoveTowardsTarget_Update()
    {
        var dir = GetDirToPlayer();

        if (dir.magnitude < RunAtPlayerDistanceLimit)
        {
            base.MoveTowardsTarget_Update();
        }

        if (dir.magnitude >= RunAtPlayerDistanceLimit)
        {
            moveBehaviour.MoveTowards(agent, GetGoblinMove());
        }
    }

    public virtual Vector3 GetGoblinMove()
    {
        if (moveBehaviour == null) { return transform.position; }

        var playerAim = Target.GetComponentInChildren<PlayerMouseAim>();
        var aimDir = Vector3.zero;

        if (playerAim != null)
        {
            aimDir = playerAim.Direction;
        }

        var dir = (Target.transform.position - agent.transform.position).x0z().normalized;

        var aimDiff = aimDir.normalized.x0z() + dir;

        //if player is aiming at gnoblin

        if (aimDiff.magnitude < .8f) { dir *= Random.Range(RunAwayMaxDist /2f, RunAwayMaxDist); dir += GetSideMoveGoblin(); }

        var moveTarget = Target.transform.position - (dir * .6f);

        return moveTarget;
    }

    public Vector3 GetSideMoveGoblin()
    {
        var dir = (Target.transform.position - agent.transform.position).x0z().normalized;

        if (dir == Vector3.zero) { dir = Vector3.right; }

        var perpendicular = new Vector3(-dir.z, 0f, dir.x);
        var range = Random.Range(-RunAwayMaxDist /2f, RunAwayMaxDist);

        return perpendicular * range;
    }
}
