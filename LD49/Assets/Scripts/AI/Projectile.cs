using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludum.AI
{
    public class Projectile : MonoBehaviour
    {
        public float m_damageOnHit { get; internal set; }

        public GameObject SpawnOnHitFX;

        public float TimeOut = 10f;

        public bool DestroyOnHit = true;

        private float m_startTime = 0f;
        private Rigidbody m_rigid;

        private void Awake()
        {
            m_startTime = Time.time;
            m_rigid = transform.GetComponentInParent<Rigidbody>();
            if (m_rigid == null) { m_rigid = gameObject.AddComponent<Rigidbody>(); }
        }

        public void SetProjectileParams(Vector3 dir, float speed)
        {
            m_rigid.velocity = dir * speed;
        }

        private void Update()
        {
            if (Time.time - m_startTime > TimeOut) { OnCollisionEnter(null); }
        }

        private void OnTriggerEnter(Collider other)
        {
            OnWhatever(other);

        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision == null || collision.collider == null) { return; }
            OnWhatever(collision.collider);
        }

        void OnWhatever(Collider other)
        {
            if (Time.time - m_startTime < .2f) { return; }

            Health health = other?.gameObject?.GetComponent<Health>();

            if (health)
            {
                health.Change(-m_damageOnHit, DamageType.Kinetic);
            }

            if (other != null)
            {
                GameObject.Instantiate(SpawnOnHitFX, transform.position, transform.rotation, null);
            }

            if (DestroyOnHit)
            {
                Destroy(gameObject);
            }
        }
    }
}
