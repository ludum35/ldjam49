using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DungeonSelect : MonoBehaviour
{
    public GameObject m_panel;

    List<Button> buttons;

    public static int numClears = 0;


    public static DungeonSelect Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void Show()
    {
        m_panel.SetActive(true);
    }
    

    private void Start()
    {
        buttons = GetComponentsInChildren<Button>(true).ToList();

        foreach(Button btn in buttons)
        {
            float difficulty = UnityEngine.Random.Range(5.0f, 10.0f + numClears * 5.0f);
            int numRooms = UnityEngine.Random.Range(3, 6 + numClears * 3);

            string difficultyStr = "";
            if(difficulty < 7.0f)
                difficultyStr = "Easy";
            else if(difficulty < 14.0f)
                difficultyStr = "Normal";
            else if(difficulty < 21.0f)
                difficultyStr = "Hard";
            else 
                difficultyStr = "Very hard";

            btn.GetComponentInChildren<TextMeshProUGUI>().text = $"{difficultyStr} dungeon. {numRooms - 2} to {numRooms + 2} rooms.";

         
            btn.onClick.AddListener(() =>
            {
                DifficultyController.SpawnPoints = difficulty;
                DungeonController.m_roomsLeft = numRooms;
                DungeonController.Instance.ExitLevel();
            });
        }
    }
}
