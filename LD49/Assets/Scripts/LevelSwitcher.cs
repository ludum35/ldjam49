using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSwitcher : MonoBehaviour
{
    Transform player;
    bool switched;

    public bool m_isDungeonEntrance;
    public Light m_openedLight;
    public Transform m_entrancePoint;

    public bool CameFrom { get; set; }

    private void OnDrawGizmos()
    {
        if(m_entrancePoint)
            Gizmos.DrawWireSphere(m_entrancePoint.position, 0.25f);
    }

    private void Start()
    {
        player = FindObjectOfType<PlayerController>().transform;
        m_openedLight.enabled = false;
    }

    private void FixedUpdate()
    {
        bool canLeave = DungeonController.Instance.CanLeave;
        if(CameFrom && !m_isDungeonEntrance)
            canLeave = false;

        if(!m_openedLight.enabled && canLeave)
        {
            m_openedLight.intensity = 0.0f;
            m_openedLight.enabled = true;
            m_openedLight.DOIntensity(100.0f, 0.5f);
        }

        if(canLeave)
        {
            Collider collider = GetComponent<Collider>();
            Vector3 pt = collider.ClosestPoint(player.position);

            if(Vector3.Distance(pt, player.position) < 0.5f)
            {
                if(!switched)
                {
                    switched = true;
                    FadeUI.Instance.FadeOut(() =>
                    {
                        if(m_isDungeonEntrance)
                        {
                            PlayerController.SetMaxHP();
                            DungeonSelect.Instance.Show();
                        }
                        else
                        {
                            PlayerController.LastLevelHealth = PlayerController.GetCurrentHealth();
                            DungeonController.Instance.ExitLevel();
                        }
                    });
                }
            }
        }
    }
}
