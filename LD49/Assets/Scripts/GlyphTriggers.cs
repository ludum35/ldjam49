﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public interface IGlyphFireTrigger { }
public interface IGlyphOnFireListener { }


public interface IGlyphHitTrigger { }
public interface IGlyphOnHitListener { }



public interface IGlyphHitWallTrigger { }
public interface IGlyphOnHitWallListener { }



public interface IGlyphSplitTrigger { }
public interface IGlyphOnSplitListener { }



public interface IGlyphMovementModifier
{
    public void Tick(GlyphBehaviour instance);
}

// Trigger normal invoke only once.
public interface ITriggerOnce { }

public interface IDoNotInherit { } 