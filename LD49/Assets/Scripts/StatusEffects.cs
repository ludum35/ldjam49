﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageType
{
    Fire,
    Lightning,
    Kinetic,
    Cold,
    Acid
}

public enum StatusEffectType
{
    None,
    Fire,
    Water,
    Oil,
    Cold,
    Acid
}

public enum StatusEffectCondition
{
    IsOnFire,
    IsWet,
    IsOiled,
    IsCold,
    IsAcid,
    IsFrozen
}

[RequireComponent(typeof(Health))]
public class StatusEffects : MonoBehaviour
{
    public List<StatusEffectCondition> GetStatusEffects()
    {
        List<StatusEffectCondition> conditions = new List<StatusEffectCondition>();

        if (IsOnFire) { conditions.Add(StatusEffectCondition.IsOnFire); }
        if (IsWet) { conditions.Add(StatusEffectCondition.IsWet); }
        if (IsOiled) { conditions.Add(StatusEffectCondition.IsOiled); }
        if (IsCold) { conditions.Add(StatusEffectCondition.IsCold); }
        if (IsAcid) { conditions.Add(StatusEffectCondition.IsAcid); }
        if (IsFrozen) { conditions.Add(StatusEffectCondition.IsFrozen); }

        return conditions;
    }

    public bool IsOnFire { get { return m_onFireFX.activeSelf; } }
    public bool IsWet { get { return m_wetFX.activeSelf; } }
    public bool IsOiled { get { return m_oiledFX.activeSelf; } }
    public bool IsCold { get { return m_coldFX.activeSelf; } }
    public bool IsAcid { get { return m_acidFX.activeSelf; } }
    public bool IsFrozen { get { return m_frozenFX.activeSelf; } }

    private Health m_health;

    public GameObject m_onFireFX;
    public GameObject m_wetFX;
    public GameObject m_frozenFX;
    public GameObject m_oiledFX;
    public GameObject m_coldFX;
    public GameObject m_acidFX;

    public event Action<StatusEffectType> OnTypeAdded;

    private float m_fireTimer;
    private float m_fireDamageTimer;
    private float m_acidDamageTimer;

    void Start()
    {
        m_health = GetComponent<Health>();
    }

    private void Update()
    {
        if(m_fireTimer > 0.0f)
        {
            m_fireTimer -= Time.deltaTime;

            if(m_fireTimer <= 0.0f)
            {
                m_onFireFX.SetActive(false);
            }
        }

        if(IsOnFire)
        {
            m_fireDamageTimer += Time.deltaTime;
            if(m_fireDamageTimer >= 1.0f)
            {
                m_health.Change(-3.0f, DamageType.Fire);
                m_fireDamageTimer -= 1.0f;
            }
        }

        if(IsAcid)
        {
            m_acidDamageTimer += Time.deltaTime;
            if(m_acidDamageTimer >= 1.0f)
            {
                m_health.Change(-1.0f, DamageType.Acid);
                m_acidDamageTimer -= 1.0f;
            }
        }
    }

    public void AddEffect(StatusEffectType type)
    {
        switch (type)
        {
            case StatusEffectType.Fire:
                if (IsWet)
                    m_wetFX.SetActive(false);
                else
                {
                    m_fireTimer = IsOiled ? 6.0f : 3.0f;
                    m_onFireFX.SetActive(true);
                }

                m_oiledFX.SetActive(false);
                m_coldFX.SetActive(false);
                m_acidFX.SetActive(false);
                break;

            case StatusEffectType.Water:
                if (IsOnFire)
                    m_onFireFX.SetActive(false);
                else if (IsCold)
                    m_frozenFX.SetActive(true);
                else
                    m_wetFX.SetActive(true);

                m_coldFX.SetActive(false);
                m_oiledFX.SetActive(false);
                m_acidFX.SetActive(false);
                break;

            case StatusEffectType.Oil:
                if (!IsOnFire && !IsWet)
                    m_oiledFX.SetActive(true);
                break;

            case StatusEffectType.Cold:
                if (IsWet)
                {
                    m_wetFX.SetActive(false);
                    m_frozenFX.SetActive(true);
                }
                else if (!m_onFireFX)
                    m_coldFX.SetActive(true);
                break;

            case StatusEffectType.Acid:

                if (!IsWet && !IsOnFire)
                    m_acidFX.SetActive(true);

                break;
        }

        OnTypeAdded?.Invoke(type);
    }


    public float GetResistance(DamageType type)
    {
        switch (type)
        {
            case DamageType.Fire:
                if (IsOiled)
                    return -0.2f;
                else if (IsWet)
                    return 0.9f;
                else if (IsCold)
                    return 0.25f;
                return 0.0f;

            case DamageType.Lightning:
                if (IsAcid || IsWet || IsOiled)
                    return -0.5f;
                return 0.0f;

            case DamageType.Kinetic:
                return 0.0f;
        }

        return 0.0f;
    }
}
