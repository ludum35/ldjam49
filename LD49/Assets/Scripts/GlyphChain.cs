using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TriggerSource
{
    Normal,
    Fire,
    Hit,
    HitWall,
    Split
}

public class GlyphChain
{
    public List<Glyph> Glyphs { get; set; }

    

    public GlyphChain()
    {
        Glyphs = new List<Glyph>();
    }

    public void RaiseFireEvent(CastSpellEvent evt)
    {
        foreach(Glyph glyph in Glyphs)
        {
            if(glyph is IGlyphOnFireListener)
                glyph.Trigger(evt, TriggerSource.Fire);
        }
    }

    public void RaiseHitEvent(CastSpellEvent evt)
    {
        foreach(Glyph glyph in Glyphs)
        {
            if(glyph is IGlyphOnHitListener)
                glyph.Trigger(evt, TriggerSource.Hit);
        }
    }

    public void RaiseWallHitEvent(CastSpellEvent evt)
    {
        foreach(Glyph glyph in Glyphs)
        {
            if(glyph is IGlyphOnHitWallListener)
                glyph.Trigger(evt, TriggerSource.HitWall);
        }
    }


    public void Trigger(CastSpellEvent evt)
    {
        evt.depth++;
        if(evt.depth >= 64)
            return;

        foreach(Glyph glyph in Glyphs)
        {
            if(!evt.ignore.Contains(glyph))
            { 
                if(glyph is ITriggerOnce)
                    evt.ignore.Add(glyph);

                glyph.Trigger(evt, TriggerSource.Normal);

                if(glyph is IGlyphFireTrigger)
                    RaiseFireEvent(evt);
            }
        }
    }


    // position -1 appends at end
    public void AddGlyph(Glyph glyph, int position = -1)
    {
        glyph.Parent = this;

        if(position > Glyphs.Count || position == -1) position = Glyphs.Count;
        if(position < 0) position = 0;

        Glyphs.Insert(position, glyph);
    }
}
