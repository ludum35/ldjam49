using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CastSpellEvent
{



    public Vector3 origin;
    public Vector3 direction;
    public GlyphChain currentChain;
    public SpellTree tree;
    public Collision lastCollision;
    internal Vector3 lastCollisionSurfaceNormal;
    public int depth;
    public List<IGlyphMovementModifier> movementModifiers;
    public GlyphBehaviour thisInstance;
    public HashSet<Glyph> ignore;
    public int charges;
    public float instability;

    public int RootEventID { get; private set; }
    public int EventID { get; private set; }
    public float SpeedMod { get; set; } = 1.0f;
    public float SizeMod { get; set; } = 1.0f;
    public float DamageMod { get; set; } = 1.0f;
    public int BouncesLeft { get; set; }

    static int s_maxEventID;

    public float GetSpeedMod()
    {
        return SpeedMod + UnityEngine.Random.Range(0.0f, Mathf.Log10(1.0f + instability) * 0.5f);
    }

    public Quaternion GetInaccuracy(float baseAccuracy)
    {
        return Quaternion.AngleAxis(UnityEngine.Random.Range(-(baseAccuracy + instability * 3.0f), baseAccuracy + instability * 3.0f), Vector3.up);
    }

    public float GetSizeMod()
    {
        float result = 1.0f;
        if(instability < 1.0f)
            result = SizeMod + UnityEngine.Random.Range(0.1f, instability);
        else 
            result = SizeMod + UnityEngine.Random.Range(1.0f / instability, 1.0f + Mathf.Log10(1.0f + instability) * 2.5f);

        return Mathf.Clamp(result, 0.1f, 2.5f);
    }

    public float GetDamageMod()
    {
        return DamageMod + UnityEngine.Random.Range(0.0f, Mathf.Log10(1.0f + instability) * 4.0f);
    }


    public CastSpellEvent(Vector3 orig, Vector3 dir, GlyphChain root, SpellTree tree, int numCharges, float totalCharge)
    {
        origin = orig;
        direction = dir;
        currentChain = root;
        this.tree = tree;
        movementModifiers = new List<IGlyphMovementModifier>();
        RootEventID = EventID = s_maxEventID++;
        ignore = new HashSet<Glyph>();
        charges = numCharges;
        instability = totalCharge;
    }

    public CastSpellEvent Copy()
    {
        CastSpellEvent result = new CastSpellEvent(origin, direction, currentChain, tree, charges - 1, instability);
        result.lastCollision = lastCollision;
        result.depth = depth;
        result.SpeedMod = SpeedMod;
        result.SizeMod = SizeMod;
        result.DamageMod = DamageMod;
        result.movementModifiers = new List<IGlyphMovementModifier>(movementModifiers);
        result.movementModifiers.RemoveAll(g => g is IDoNotInherit);
        result.ignore = new HashSet<Glyph>(ignore);
        result.BouncesLeft = BouncesLeft;
        result.EventID = s_maxEventID++;
        return result;
    }
}

public class SpellTree : MonoBehaviour
{

    public Vector3 dir;
    public bool UseTransformForwards = true;

    public GameObject m_fireballPrefab;
    public float m_fireballSpeed;
    [Space(20.0f)]
    public GameObject m_frostboltPrefab;
    public float m_frostboltSpeed;
    [Space(20.0f)]
    public GameObject m_explosionPrefab;
    [Space(20.0f)]
    public GameObject m_acidSprayPrefab;
    [Space(20.0f)]
    public GameObject m_shrapnelFragmentPrefab;
    [Space(20.0f)]
    public GameObject m_flameSprayPrefab;
    [Space(20.0f)]
    public GameObject m_waterSprayPrefab;
    [Space(20.0f)]
    public GameObject m_oilSprayPrefab;
    [Space(20.0f)]
    public GameObject m_lightningPrefab;
    [Space(20.0f)]
    public GameObject m_iceblastPrefab;
    [Space(20.0f)]
    public GameObject m_magicBoltPrefab;
    public float m_magicBoltSpeed = 10.0f;
    public GameObject textfx;



    static GlyphChain m_root;
    NPCChoiceTable m_glyphGen;
    NPCChoiceTable m_glyphGen2;

    Glyph m_generatedGlyph;
    public float m_cooldownTimer = 0.0f;

    public float m_castCooldown = 1.0f;

    public GlyphChain Root { get { return m_root; } }

    public event Action OnAttack;

    public HashSet<Glyph> NewGlyphs { get; set; }

    void Start()
    {
        if(m_root == null)
        {
            ResetTree();

            /*
            m_root.AddGlyph(new Grow());
            m_root.AddGlyph(new Fireball());
            m_root.AddGlyph(new Bounce());
            //
            //GlyphChain trailChain2 = new GlyphChain();
            //trailChain2.AddGlyph(new Spray() { Type = SprayType.Flames });
            //m_root.AddGlyph(new Trail() { Chain = trailChain2 });
            //
            GlyphChain trailChain = new GlyphChain();
            trailChain.AddGlyph(new Shrapnel());
            m_root.AddGlyph(new Trail() { Chain = trailChain });
            //
            //GlyphChain onHitChain = new GlyphChain();
            //onHitChain.AddGlyph(new Spray() { Type = SprayType.LightningBlast });
            //trailChain.AddGlyph(new OnHitChain() { Chain = onHitChain });
            */
        }


        new SpawnPool();

        NewGlyphs = new HashSet<Glyph>();

        m_glyphGen = new NPCChoiceTable();
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new Bounce(); });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new Explosion(); });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new Fireball(); });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new FrostBolt(); });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new MagicBolt(); });
        m_glyphGen.AddChoice(3, () => { m_generatedGlyph = new Gravity() { Mode = Gravity.GravityMode.Backwards }; });
        m_glyphGen.AddChoice(3, () => { m_generatedGlyph = new Gravity() { Mode = Gravity.GravityMode.Straight }; });
        m_glyphGen.AddChoice(3, () => { m_generatedGlyph = new Gravity() { Mode = Gravity.GravityMode.Left }; });
        m_glyphGen.AddChoice(3, () => { m_generatedGlyph = new Gravity() { Mode = Gravity.GravityMode.Right }; });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new Grow(); });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new MoreDamage(); });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new Shrapnel(); });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new Shrink(); });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new SlowDown(); });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new SpeedUp(); });
        m_glyphGen.AddChoice(5, () => { m_generatedGlyph = new Split() { Shape = Split.SplitShape.Diagonal2 }; });
        m_glyphGen.AddChoice(5, () => { m_generatedGlyph = new Split() { Shape = Split.SplitShape.Diagonal4 }; });
        m_glyphGen.AddChoice(5, () => { m_generatedGlyph = new Split() { Shape = Split.SplitShape.Side }; });
        m_glyphGen.AddChoice(5, () => { m_generatedGlyph = new Spray() { Type = SprayType.Acid }; });
        m_glyphGen.AddChoice(5, () => { m_generatedGlyph = new Spray() { Type = SprayType.Flames }; });
        m_glyphGen.AddChoice(5, () => { m_generatedGlyph = new Spray() { Type = SprayType.IceBlast }; });
        m_glyphGen.AddChoice(5, () => { m_generatedGlyph = new Spray() { Type = SprayType.LightningBlast }; });
        m_glyphGen.AddChoice(5, () => { m_generatedGlyph = new Spray() { Type = SprayType.Oil }; });
        m_glyphGen.AddChoice(5, () => { m_generatedGlyph = new Spray() { Type = SprayType.Water }; });
        m_glyphGen.AddChoice(10, () => { m_generatedGlyph = new WigglyBoy(); });
        // Chance of new chains
        m_glyphGen.AddChoice(25, () => { m_generatedGlyph = new OnFireChain(); });
        m_glyphGen.AddChoice(25, () => { m_generatedGlyph = new OnHitChain(); });
        m_glyphGen.AddChoice(25, () => { m_generatedGlyph = new Trail(); });

        m_glyphGen2 = new NPCChoiceTable();
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new Bounce(); });
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new Explosion(); });
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new Fireball(); });
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new FrostBolt(); });
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new MagicBolt(); });
        m_glyphGen2.AddChoice(3, () => { m_generatedGlyph = new Gravity() { Mode = Gravity.GravityMode.Backwards }; });
        m_glyphGen2.AddChoice(3, () => { m_generatedGlyph = new Gravity() { Mode = Gravity.GravityMode.Straight }; });
        m_glyphGen2.AddChoice(3, () => { m_generatedGlyph = new Gravity() { Mode = Gravity.GravityMode.Left }; });
        m_glyphGen2.AddChoice(3, () => { m_generatedGlyph = new Gravity() { Mode = Gravity.GravityMode.Right }; });
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new Grow(); });
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new MoreDamage(); });
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new Shrink(); });
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new SlowDown(); });
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new SpeedUp(); });
        m_glyphGen2.AddChoice(10, () => { m_generatedGlyph = new WigglyBoy(); });
        // Chance of new chains
        m_glyphGen2.AddChoice(25, () => { m_generatedGlyph = new OnFireChain(); });
        m_glyphGen2.AddChoice(25, () => { m_generatedGlyph = new OnHitChain(); });
        m_glyphGen2.AddChoice(25, () => { m_generatedGlyph = new Trail(); });
    }

    public void ResetTree()
    {
        m_root = new GlyphChain();

        m_root.AddGlyph(new MagicBolt());
    }

    public void Update()
    {
        if(UseTransformForwards)
        {
            dir = transform.forward;
        }

        m_cooldownTimer -= Time.deltaTime;

    }

    public void Fire(int numCharges, float totalCharge)
    {
        if(m_cooldownTimer < 0.0f)
        { 
            OnAttack?.Invoke();
            CastSpellEvent evt = new CastSpellEvent(transform.position + Vector3.up * 0.1f, dir.normalized, m_root, this, numCharges, totalCharge);
            m_root.Trigger(evt);

            m_cooldownTimer = m_castCooldown;
        }
    }

    public void AddRandomGlyph()
    {
        
        List<GlyphChain> chains = new List<GlyphChain>();
        Walk(m_root);


        GlyphChain chain = chains[UnityEngine.Random.Range(0, chains.Count)];

        if(chain == m_root)
        {
            m_glyphGen2.Execute();
            chain.AddGlyph(m_generatedGlyph);
        }
        else
        {
            m_glyphGen.Execute();
            chain.AddGlyph(m_generatedGlyph);
        }

        NewGlyphs.Add(m_generatedGlyph);




        string idtext = m_generatedGlyph.GetName();
        Sprite idsprite = m_generatedGlyph.GetSprite();
        Instantiate(textfx, new Vector3(100, 0, 0), Quaternion.identity, GameObject.Find("PlayerUI").transform);
        Image runeimage = textfx.GetComponentInChildren<Image>();
        runeimage.sprite = idsprite;
        Text runetext = textfx.GetComponentInChildren<Text>();
        runetext.text = idtext;



        void Walk(GlyphChain current)
        {
            chains.Add(current);

            foreach(Glyph glyph in current.Glyphs)
            {
                if(glyph.GetSubChain() != null)
                    Walk(glyph.GetSubChain());
                


            }

        }



            


 




    }

}
