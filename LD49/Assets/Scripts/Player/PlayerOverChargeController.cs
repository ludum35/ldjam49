using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOverChargeController : MonoBehaviour
{
    public float ChargeMultiplier;
    public float MaxCharge;
    public float Charge;
    public int ChargedCount;

    private bool m_isCharging = false;

    public event Action OnChargeStarted;
    public event Action OnChargeReleased;
    public event Action OnChargeLevel;


    // Update is called once per frame
    void Update()
    {
        UpdateCharge();
    }

    private void UpdateCharge()
    {
        if (Input.GetMouseButton(0) && GetComponent<PlayerController>().SpellTree.m_cooldownTimer < 0.0f)
        {
            if (m_isCharging == false) { OnChargeStarted?.Invoke(); }

            m_isCharging = true;
            Charge += ChargeMultiplier * Time.deltaTime;

            if(Charge >= MaxCharge)
            { 
                Charge -= MaxCharge;
                OnChargeLevel?.Invoke();
                ChargedCount++;
            }
        }

        if(Input.GetMouseButtonUp(0))
        {
            GetComponent<PlayerController>().SpellTree.Fire(ChargedCount, (float)ChargedCount * MaxCharge + Charge);
            OnChargeReleased?.Invoke();
            m_isCharging = false;
            Charge = 0f;
            ChargedCount = 0;
        }
    }

    public float GetNormalizedCharge()
    {
        return Mathf.InverseLerp(0f, MaxCharge, Charge);
    }
}
