using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class PlayerOverChargeFX : MonoBehaviour
{
    public GameObject ChargeStartFX;

    public GameObject ChargeLevelOne;

    public PlayerOverChargeController Controller;

    public List<GameObject> activeChargeFX = new List<GameObject>();

    public GameObject Initial = null;

    public float maxSize = .7f;
    public float multiplier = .1f;
    public float cameraShakeMultiplier = 10f;
    public SimpleCameraShake shake;

    private void Start()
    {
        shake = FindObjectOfType<SimpleCameraShake>();
        Controller.OnChargeStarted += OnChargeStarted;
        Controller.OnChargeLevel += OnChargeLevel;
        Controller.OnChargeReleased += OnChargeReleased;
    }

    private void OnChargeLevel()
    {
        float charge = Controller.ChargedCount;
        if (charge == 0) { return; }
        if (Initial != null)
        {
            var size = Vector3.one * charge * multiplier;
            if (size.x > maxSize) { size = Vector3.one * maxSize; }
            shake.AddShake(size.magnitude * cameraShakeMultiplier);
            Initial.transform.DOScale(size, .1f);
        }
    }

    public void OnChargeReleased()
    {
        foreach (var k in activeChargeFX)
        {
            Destroy(k.gameObject);
        }

        activeChargeFX.Clear();
    }

    public void OnChargeStarted()
    {
        Initial = Instantiate(ChargeStartFX, transform.position, Quaternion.identity, transform);
        Initial.transform.localScale = Vector3.zero;
        Initial.transform.DOScale(.1f, .1f);
        activeChargeFX.Add(Initial);
    }
}
