using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController ActiveController;

    public PlayerMouseAim MouseAim;
    public SpellTree SpellTree;

    public static float MaxHP = 30.0f;
    public static float LastLevelHealth = MaxHP;

    public Health m_HP;

    private void Start()
    {
        m_HP = GetComponent<Health>();
        LevelSwitcher[] switchers = FindObjectsOfType<LevelSwitcher>();

        LevelSwitcher switcher = switchers[UnityEngine.Random.Range(0, switchers.Length)];
        switcher.CameFrom = true;

        ActiveController = this;
        MaxHP = m_HP.m_max;

        transform.position = switcher.m_entrancePoint.position;
    }


    // Update is called once per frame
    void Update()
    {
        SpellTree.dir = MouseAim.Direction;
        SpellTree.transform.localPosition = Vector3.up * 0.2f + SpellTree.dir.normalized * 0.5f;

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SpellEditorUI.Instance.Toggle(SpellTree);
        }

        if (DungeonController.IsInDungeon)
        {
            if (m_HP.Current <= 0f)
            {
                DungeonController.Instance.OnGameOver();
            }
        }
    }

    public static float GetCurrentHealth()
    {
        return ActiveController?.GetComponent<Health>()?.Current ?? MaxHP;
    }

    public static void SetMaxHP()
    {
        LastLevelHealth = MaxHP;
    }
}
