using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerAnimController : MonoBehaviour
{
    public string Walking = "Walking";
    public string Attacking = "Attacking";
    public string MoveSpeed = "MoveSpeed";

    public CharacterController m_charControl;
    public SpellTree m_attackSource;

    public Animator m_animator;
    // Update is called once per frame

    public bool m_isAttacking = false;
    public float moveSpeedMultiplier = 1f;
    public float lastAttackTime = 0f;
    public float attackAnimTime = .2f;

    public float StepInterval = .2f;
    public float lastStepTime = 0f;
    public UnityEvent OnStep;
    public UnityEvent OnMagicCast;
    private void Start()
    {
        m_attackSource.OnAttack += OnAttackTrigger;
    }

    private void OnAttackTrigger()
    {
        OnMagicCast?.Invoke();
        lastAttackTime = Time.time;
    }

    void Update()
    {
        UpdateAnimStates();
    }

    void UpdateAnimStates()
    {
        m_animator.SetFloat(MoveSpeed, .2f + (m_charControl.velocity.magnitude * moveSpeedMultiplier));

        var isWalking = m_charControl.velocity.magnitude > .15f;
        m_animator.SetBool(Walking, isWalking);

        if (isWalking)
        {
            if (Time.time - StepInterval > lastStepTime)
            {
                OnStep?.Invoke();
                lastStepTime = Time.time;
            }
        }

        if (Time.time - lastAttackTime > attackAnimTime) { m_isAttacking = false; }
        else { m_isAttacking = true; }

        m_animator.SetBool(Attacking, m_isAttacking);
    }
}
