using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyController : MonoBehaviour
{
    /// <summary>
    /// Number of points to use for spawning kreeps.
    /// </summary>
    public static float SpawnPoints = 5f;


    public void Start()
    {
        SpawnEnemiesForAllPoints();
    }

    public void SpawnEnemiesForAllPoints()
    {
        var spawners = FindObjectsOfType<EnemySpawner>();

        var pointsPerSpawner = SpawnPoints / spawners.Length;

        foreach (var k in spawners)
        {
            k.SpawnForPoints(pointsPerSpawner);
        }
    }
}
