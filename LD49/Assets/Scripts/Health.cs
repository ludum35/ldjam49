﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public float m_max;

    public bool m_isPlayer;

    [SerializeField]
    private float m_CurrentHP; // editor readonly

    private StatusEffects m_status;

    public float Current { get; private set; }
    public Action OnDeath { get; set; }

    public UnityEvent OnGetHit;

    public void Start()
    {
        Current = m_max;
        if (m_isPlayer)
            Current = PlayerController.LastLevelHealth;


        m_status = GetComponent<StatusEffects>();

    }

    public void Change(float amount, DamageType type)
    {
        if (m_status != null)
        {
            if (amount < 0.0f)
            {
                float resistance = m_status.GetResistance(type);
                amount *= (1.0f - resistance);
            }
        }

        if (amount <= -.5f) { OnGetHit?.Invoke(); }

        Current += amount;
        Current = Mathf.Min(m_max, Current);
        Current = Mathf.Max(0, Current);

        m_CurrentHP = Current;

        if (Current == 0.0f)
        {
            // Die or something I dont know
            OnDeath?.Invoke();
        }
    }

    public void ApplyStatus(StatusEffectType type)
    {
        if (m_status != null)
        {
            m_status.AddEffect(type);
        }
    }
}
