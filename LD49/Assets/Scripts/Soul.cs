using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soul : MonoBehaviour
{
    public AnimationCurve m_curve;

    PlayerController m_player;
    Vector3 m_origin;
    float m_t;

    private void Start()
    {
        m_player = FindObjectOfType<PlayerController>();
        m_origin = transform.position;
    }


    private void FixedUpdate()
    {        
        float t = m_curve.Evaluate(m_t);

        transform.position = Vector3.Lerp(m_origin, m_player.transform.position, t);

        m_t += Time.deltaTime;

        if(m_t >= 1.0f)
        {
            Destroy(gameObject);
            m_player.SpellTree.AddRandomGlyph();
        }
    }
}
