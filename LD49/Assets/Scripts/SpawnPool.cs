﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnPool
{
    // Mapping of prefab to instances
    private Dictionary<GameObject, HashSet<GameObject>> m_availableInstances;
    private Dictionary<GameObject, HashSet<GameObject>> m_unavailableInstances;
    // Mapping of prefab instance to its source prefab.
    private Dictionary<GameObject, GameObject> m_sourceMapping;

    private GameObject m_pooledParent;

    public static SpawnPool Instance { get; private set; }

    public SpawnPool()
    {
        if(Instance == null)
        { 
            m_availableInstances = new Dictionary<GameObject, HashSet<GameObject>>();
            m_unavailableInstances = new Dictionary<GameObject, HashSet<GameObject>>();
            m_sourceMapping = new Dictionary<GameObject, GameObject>();

            m_pooledParent = new GameObject("Pooled");
            GameObject.DontDestroyOnLoad(m_pooledParent);

            Instance = this;
        }
    }


    public GameObject Instantiate(GameObject prefab)
    {
        return Instantiate(prefab, Vector3.one, Quaternion.identity, null);
    }

    public GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent)
    {
        if(m_availableInstances.ContainsKey(prefab))
        {
            HashSet<GameObject> available = m_availableInstances[prefab];
            if(available.Count > 0)
            {
                GameObject result = available.Last();
                result.transform.SetParent(parent);
                result.transform.position = position;
                result.transform.rotation = rotation;
                result.SetActive(true);

                m_unavailableInstances[prefab].Add(result);
                available.Remove(result);

                return result;
            }
        }
        else
        {
            m_availableInstances.Add(prefab, new HashSet<GameObject>());
            m_unavailableInstances.Add(prefab, new HashSet<GameObject>());
        }


        GameObject newInstance = GameObject.Instantiate(prefab, position, rotation, parent);
        m_unavailableInstances[prefab].Add(newInstance);
        m_sourceMapping.Add(newInstance, prefab);
        return newInstance;
    }

    public void Destroy(GameObject obj)
    {
        if(!m_sourceMapping.ContainsKey(obj))
            return;

        GameObject origin = m_sourceMapping[obj];

        if(m_availableInstances[origin].Contains(obj))
            return;

        m_unavailableInstances[origin].Remove(obj);
        m_availableInstances[origin].Add(obj);
        obj.transform.SetParent(m_pooledParent.transform);
        obj.SetActive(false);
    }
}