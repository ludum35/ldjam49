using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class AIAnimationController : MonoBehaviour
{
    public string Walking = "Walking";
    public string Attacking = "Attacking";
    public string MoveSpeed = "MoveSpeed";

    public NavMeshAgent navMeshAgent;
    public AttackBehaviour attackSource;
    public Animator m_animator;
    // Update is called once per frame

    public bool m_isAttacking = false;
    public float moveSpeedMultiplier = 1f;
    public float lastAttackTime = 0f;
    public float attackAnimTime = .2f;

    public float StepInterval = .2f;

    public float lastStepTime = 0f;

    public UnityEvent OnStep;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        attackSource = GetComponent<AttackBehaviour>();
        m_animator = transform.GetComponentInChildren<Animator>();

        attackSource.OnAttackStarted += OnAttackTrigger;
    }

    private void OnAttackTrigger()
    {
        lastAttackTime = Time.time;
    }

    void Update()
    {
        UpdateAnimStates();
    }

    void UpdateAnimStates()
    {
        m_animator.SetFloat(MoveSpeed, .2f + (navMeshAgent.velocity.magnitude * moveSpeedMultiplier));

        var isWalking = navMeshAgent.velocity.magnitude > .15f;

        if (isWalking)
        {
            if (Time.time - StepInterval > lastStepTime)
            {
                OnStep?.Invoke();
                lastStepTime = Time.time;
            }
        }
        
        m_animator.SetBool(Walking, isWalking);

        if (Time.time - lastAttackTime > attackAnimTime) { m_isAttacking = false; }
        else { m_isAttacking = true; }

        m_animator.SetBool(Attacking, m_isAttacking);
    }
}
