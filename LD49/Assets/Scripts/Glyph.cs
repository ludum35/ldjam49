using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Glyph
{
    public GlyphChain Parent { get; set; }

    public abstract void Trigger(CastSpellEvent evt, TriggerSource source);

    public abstract string GetName();
    public abstract string GetDescription();
    public abstract Sprite GetSprite();

    public virtual GlyphChain GetSubChain() { return null; }
}
