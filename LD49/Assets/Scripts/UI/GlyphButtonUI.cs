﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GlyphButtonUI : MonoBehaviour
{
    public TextMeshProUGUI m_text;
    public Image m_image;

    public static GlyphButtonUI Dragging { get; private set; }
    public static GameObject DraggingInstance { get; private set; }

    public Glyph Glyph { get; private set; }

    public bool interactable;

    public void Display(Glyph glyph)
    {
        //m_text.text = glyph.GetName();

        Glyph = glyph;

        m_image.sprite = glyph.GetSprite();

        interactable = SpellEditorUI.Instance.IsEditable;

        GlyphChain chain = glyph.GetSubChain();  
        if(chain != null && chain.Glyphs.Count > 0)
        {
            GetComponent<Button>().interactable = false;
            interactable = false;
        }

        if(GetComponentInParent<SpellEditorUI>().m_spellTree.NewGlyphs.Contains(glyph))
        {
            Outline outline = gameObject.AddComponent<Outline>();
            outline.effectColor = Color.blue;
        }
    }

    public void BeginDrag()
    {
        if(interactable && SpellEditorUI.Instance.IsEditable)
        { 
            Dragging = this;
            DraggingInstance = Instantiate(gameObject);

            DraggingInstance.transform.SetParent(SpellEditorUI.Instance.transform);
            DraggingInstance.AddComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void Drag()
    {
        if(interactable && DraggingInstance != null)
        { 
            if(GlyphChainUI.HoveredChain == null)
                DraggingInstance.transform.position = Input.mousePosition;
        }
    }

    public void EndDrag()
    {
        if(SpellEditorUI.Instance.IsEditable)
        {
            if(GlyphChainUI.HoveredChain != null && GlyphChainUI.HoveredChain.Chain != Dragging.Glyph.Parent)
            { 
                Glyph glyph = Dragging.Glyph;
                GlyphChain oldChain = Dragging.Glyph.Parent;
                oldChain.Glyphs.Remove(glyph);
            
                GlyphChainUI.HoveredChain.Chain.AddGlyph(glyph);
                SpellEditorUI.Instance.Refresh();
            }

            Dragging = null;
            Destroy(DraggingInstance);
            DraggingInstance = null;
        }
    }

    public void OnPointerEnter()
    {
        if(!Dragging)
        {
            TooltipUI.Instance.Show(transform.position + new Vector3(32.0f, 0.0f, 0.0f), Glyph.GetName(), Glyph.GetDescription());
        }
    }

    public void OnPointerExit()
    {
        TooltipUI.Instance.Close();
    }
}
