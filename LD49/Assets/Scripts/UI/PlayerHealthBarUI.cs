using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBarUI : MonoBehaviour
{
    public Health playerHealth;

    public TextMeshProUGUI healthText;

    public Image fillImage;

    Tween moveTween = null;

    float lastUpdateHP;

    // Start is called before the first frame update
    void Start()
    {
        playerHealth = FindObjectOfType<PlayerController>()?.GetComponent<Health>();

        UpdateUITween();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerHealth.Current != lastUpdateHP)
        {
            UpdateUITween();
            lastUpdateHP = playerHealth.Current;
        }
    }

    public void UpdateUITween()
    {
        if (moveTween != null) { moveTween.Kill(); }
        fillImage.DOFillAmount(playerHealth.Current / playerHealth.m_max, .2f);
        string hpString = $"{playerHealth.Current:F0} / {playerHealth.m_max:F0}";
        healthText.text = hpString;
    }

    public static TweenerCore<string, string, StringOptions> DOText(TextMeshProUGUI target, string endValue, float duration, bool richTextEnabled = true, ScrambleMode scrambleMode = ScrambleMode.None, string scrambleChars = null)
    {
        if (endValue == null)
        {
            if (Debugger.logPriority > 0) Debugger.LogWarning("You can't pass a NULL string to DOText: an empty string will be used instead to avoid errors");
            endValue = "";
        }
        TweenerCore<string, string, StringOptions> t = DOTween.To(() => target.text, x => target.text = x, endValue, duration);
        t.SetOptions(richTextEnabled, scrambleMode, scrambleChars)
            .SetTarget(target);
        return t;
    }
}
