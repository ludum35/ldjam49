using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using TMPro;

public class UIOverChargeBar : MonoBehaviour
{
    public Image FillImage;

    public PlayerOverChargeController OverChargeController;

    public TextMeshProUGUI ChargeText;

    public void Awake()
    {
        OverChargeController = FindObjectOfType<PlayerOverChargeController>();
    }

    public void Update()
    {
        UpdateBar();
    }

    private void UpdateBar()
    {
        FillImage.DOFillAmount(OverChargeController.GetNormalizedCharge(), .1f);
        ChargeText.text = OverChargeController.ChargedCount.ToString();
    }
}
