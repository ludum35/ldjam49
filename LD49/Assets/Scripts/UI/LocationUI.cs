using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LocationUI : MonoBehaviour
{
    public Image ImagePanel;

    public List<Sprite> locationSprites;

    public Sprite townSprite;

    // Start is called before the first frame update
    void Start()
    {
        bool isTown = SceneManager.GetActiveScene().name.ToLowerInvariant() == "town";
        if (isTown)
        {
            ImagePanel.sprite = townSprite;
        }
        else
        {
            ImagePanel.sprite = locationSprites.Random();
        }
    }
}
