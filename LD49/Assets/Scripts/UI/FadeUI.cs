using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeUI : MonoBehaviour
{
    public static FadeUI Instance { get; set; }

    private void Awake()
    {
        Instance = this;
    }

    public void FadeOut(Action atEnd)
    {
        CanvasGroup cg = GetComponent<CanvasGroup>();
        cg.DOKill();
        cg.DOFade(1.0f, 0.25f).OnComplete(() => { atEnd?.Invoke(); });
    }

    public void FadeIn(Action atEnd)
    {
        CanvasGroup cg = GetComponent<CanvasGroup>();
        cg.DOKill();
        cg.DOFade(0.0f, 0.25f).OnComplete(() => { atEnd?.Invoke(); });
    }
}
