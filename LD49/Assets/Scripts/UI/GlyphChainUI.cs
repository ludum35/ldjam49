﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GlyphChainUI : MonoBehaviour
{
    public GameObject m_glyphButton;
    public GameObject m_spacer;
    public TextMeshProUGUI m_depthText;

    public static GlyphChainUI HoveredChain { get; private set; }

    public GlyphChain Chain { get; private set; }

    public void Display(GlyphChain chain, int depth, bool isFirstChain = false)
    {
        Chain = chain;
        bool first = true;
        m_depthText.text = depth.ToString();

        foreach(Glyph glyph in chain.Glyphs)
        {
            GameObject glyphBtn = Instantiate(m_glyphButton, transform);
            GlyphButtonUI btn = glyphBtn.GetComponent<GlyphButtonUI>();
            btn.Display(glyph);

            if(isFirstChain && first)
            {
                first = false;
                btn.GetComponent<Button>().interactable = false;
                btn.interactable = false;
            }
        }

        if(chain.Glyphs.Count == 0)
        {
            Instantiate(m_spacer).transform.SetParent(transform);
        }

    }


    public void OnPointerEnter()
    {
        HoveredChain = this;

        if(GlyphButtonUI.Dragging != null)
        {
            if(GlyphButtonUI.Dragging.Glyph.Parent != Chain)
            {
                GlyphButtonUI.DraggingInstance.transform.SetParent(transform);
            }
        }
    }

    public void OnPointerExit()
    {
        HoveredChain = null;

        if(GlyphButtonUI.Dragging != null)
        {
            if(GlyphButtonUI.Dragging.Glyph.Parent != Chain)
            {
                GlyphButtonUI.DraggingInstance.transform.SetParent(SpellEditorUI.Instance.transform);
            }
        }
    }
}
