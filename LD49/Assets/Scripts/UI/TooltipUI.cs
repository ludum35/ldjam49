using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TooltipUI : MonoBehaviour
{
    CanvasGroup cg;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI descriptionText;
    public static TooltipUI Instance;

    private void Awake()
    {
        cg = GetComponent<CanvasGroup>();
        Instance = this;
    }

    public void Close()
    {
        cg.DOKill();
        cg.alpha = 0.0f;
    }

    public void Show(Vector3 position, string nameStr, string descriptionStr)
    {
        cg.DOKill();
        cg.DOFade(1.0f, 0.25f);


        transform.GetComponent<RectTransform>().position = position;

        nameText.text = nameStr;
        descriptionText.text = descriptionStr;
    }
}
