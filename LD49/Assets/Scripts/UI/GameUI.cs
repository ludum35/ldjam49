using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameUI : Singleton<GameUI>
{
    public UIOverChargeBar UIOverChargeBar;
    public GameObject gameOverScreen;
    public GameObject GameUIHolder;

    public override void Awake()
    {
        base.Awake();

        UIOverChargeBar = FindObjectOfType<UIOverChargeBar>();
        GameUIHolder = this.gameObject;
    }

    public void ResetGame()
    {
        gameOverScreen.SetActive(false);
        FindObjectOfType<SpellTree>().ResetTree();
        SceneManager.LoadScene("Town");
    }


}
