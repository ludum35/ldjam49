﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpellEditorUI : MonoBehaviour
{
    public GameObject m_glyphChainUIPrefab;
    public Transform m_content;
    public GameObject m_arrow;
    public GameObject m_panel;
    public GameObject m_linePrefab;
    public GameObject m_warningMessage;


    [Space(20.0f)]
    public Sprite BounceSprite;
    public Sprite ExplosionSprite;
    public Sprite FireballSprite;
    public Sprite FrostBoltSprite;
    public Sprite MagicBoltSprite;
    public Sprite GravityBackwards;
    public Sprite GravityForward;
    public Sprite GravityLeft;
    public Sprite GravityRight;
    public Sprite GrowSprite;
    public Sprite MoreDamageSprite;
    public Sprite ShrapnelSprite;
    public Sprite ShrinkSprite;
    public Sprite SlowDownSprite;
    public Sprite SpeedUpSprite;
    public Sprite Split2Sprite;
    public Sprite Split4Sprite;
    public Sprite SplitSidewaysSprite;
    public Sprite SprayAcidSprite;
    public Sprite SprayFlamesSprite;
    public Sprite IceBlastSprite;
    public Sprite LightningBlastSprite;
    public Sprite SprayOilSprite;
    public Sprite SprayWaterSprite;
    public Sprite WigglyBoySprite;
    public Sprite OnFireChainSprite;
    public Sprite OnHitChainSprite;
    public Sprite TrailSprite;

    List<GameObject> m_glyphChains;
    List<GameObject> m_arrows;
    public SpellTree m_spellTree;
    bool m_firstOpen = true; 

    public static SpellEditorUI Instance { get; private set; }

    public bool IsEditable { get; private set; }

    

    private void Awake()
    {
        Instance = this;
    }

    public void Toggle(SpellTree spellTree)
    {
        IsEditable = SceneManager.GetActiveScene().name == "Town";

        if(!m_panel.activeSelf)
        {
            m_warningMessage.SetActive(!IsEditable);
            m_panel.SetActive(true);
            Display(spellTree);

            if(m_firstOpen)
            {
                m_firstOpen = false;
                m_panel.SetActive(false);
                m_panel.SetActive(true);


            }

            spellTree.NewGlyphs.Clear();
        }
        else
        {
            m_panel.SetActive(false);
        }
    }

    public void Refresh()
    {
        Display(m_spellTree);
    }

    private void Display(SpellTree spellTree)
    {
        m_spellTree = spellTree;

        if(m_glyphChains != null)
            m_glyphChains.ForEach(gc => Destroy(gc));

        if(m_arrows != null)
            m_arrows.ForEach(a => Destroy(a));

        m_glyphChains = new List<GameObject>();
        m_arrows = new List<GameObject>();

        Vector3 currentPos = new Vector3(64.0f, -64.0f, 0.0f);
        CreateChains(spellTree.Root, 0, true);


        void CreateChains(GlyphChain parentChain, int depth, bool first)
        {
            GameObject chainObj = Instantiate(m_glyphChainUIPrefab, m_content);
            GlyphChainUI chainUI = chainObj.GetComponent<GlyphChainUI>();
            chainUI.Display(parentChain, depth, first);
            m_glyphChains.Add(chainObj);

            RectTransform rt = chainObj.GetComponent<RectTransform>();
            rt.localPosition = currentPos;
            Vector3 parentPos = currentPos;

            for(int i = parentChain.Glyphs.Count - 1; i >= 0; i--)
            {
                Glyph glyph = parentChain.Glyphs[i];
                GlyphChain chain = glyph.GetSubChain();
                if(chain != null)
                {
                    currentPos.x += 80.0f;
                    currentPos.y = rt.localPosition.y - (i * 64.0f);

                    GameObject line = Instantiate(m_linePrefab, m_content);
                    RectTransform lineRt = line.GetComponent<RectTransform>();
                    lineRt.localPosition = parentPos + new Vector3(64.0f, -(i * 64.0f + 32.0f));
                    lineRt.sizeDelta = new Vector3(currentPos.x - parentPos.x - 72.0f, 2.0f, 0.0f);
                    m_arrows.Add(line);

                    CreateChains(chain, depth + 1, false);
                }

            }
        }
    }
}
